# -*- coding: utf-8 -*-
from typing import Union, List, Callable, Optional, Any
from collections import namedtuple
from dataclasses import dataclass
from lxml import etree
from datetime import datetime

import numpy as np

inquiry = lambda x: print('INQUIRY:', x, x.size, x.shape, type(x))

SarCoordinate = namedtuple('SarCoordinate', 'range_time azimuth_time')

ImageCoordinate = namedtuple('ImageCoordinate', 'x y')


@dataclass
class Poly1DList():
    """Generic class to supporting a list of 1D range polynomials.

       The polynomials are defined as:
       f(x) = c0 + c1(xp - xref) + c2(xp - xref)^2 + ...
       where xp is the range variant variable

       The selection of the polynomial is done searching the closest in time with
       respect to the input azimuth

    """
    time_tag: List[np.datetime64]
    xref: List[np.double]
    poly_list: Any

    def __call__(self, time: np.datetime64, xp: np.double):
        return self.polyval(time, xp)

    def polyval(self, time: np.datetime64, xp: np.double):
        ix = self.get_closest(time)
        xref = self.xref[ix]
        poly = self.poly_list[ix]
        #logger.debug(f"closest {ix} {xp} {xref} {poly} ")
        dum =  np.polyval (poly, xp - xref )
        #logger.debug(f"val {dum}")
        return dum

    def get_closest(self, time: np.datetime64) -> np.int:
        """
        Return the index giving of the closest polynomial to the input time
        Paramters
        ---------
        time : datetime
        Return
        ------
        int :
            closest index
        """

        dt = self.time_tag - time

        if isinstance(dt, np.timedelta64):
            dt = dt.total_seconds()
        return np.argmin(abs(dt))

    def is_valid(self):
        if self.time_tag.size == 0:
            return False
        return True

def datetime_to_mjd2000(time: Union[datetime, List[datetime]],
                        reference_time: datetime = datetime(2000, 1, 1)) -> np.ndarray:
    """
    Convert a datetime into elapased seconds since the reference time datetime(2000, 1, 1)

    Paramters
    ---------
    time: datetime,
        Input time to Convert. It can be a list of datetimes
    reference_time: datetime
        Reference datetime

    Return
    -------
    np.float64 or ndarray[np.float64]
        dtime


    """
    if hasattr(time, '__iter__'):
        dtime = [(t_ - reference_time).total_seconds() for t_ in time]
    else:
        dtime = (time - reference_time).total_seconds()
    return np.double(dtime)


def xpath_to_ndarray(root: Union[str, etree._Element],
                     xpath: Callable = str,
                     cast_type=str,
                     namespace: Optional[str] = None,
                     asarray=False):
    """ Convert an xpath into a list
        <product><biomass:a>3 </biomass:a></product>
        xp='product/biomass'
        xpath_to_list(root, xp, namespace='a', cast_type=np.double)
    Parameters
    ----------
    root: etree element str
        pointing both to an XML file or element of a XML file
    xpath: str
        xpath of the field(s) to Search
    namespace: Optional[str]=None
        namespace for the field

    cast_type: Callable
        function for casting the result. The default is a str()

    asarray: bool
        keep as array if the size of the array is 1

    """
    # print ("==c=",root, xpath, cast_type, namespace)
    if not isinstance(root, etree._Element):
        root = etree.parse(root).getroot()
    
    if namespace is None:
        namespace = root.nsmap
    # getting all the elements in a list
    ll = [elt.text for elt in root.findall(xpath, namespace)]
    ll = np.asarray(ll, dtype=cast_type)

    if not asarray:
        if ll.size == 1:
            ll = ll.item(0)
            # in the case of np.datetiem64 item(0) is casting to datetime.datetime
            # https://github.com/numpy/numpy/issues/7619
            if cast_type is np.datetime64:
                ll = np.datetime64(ll)

    return ll
