import matplotlib.pyplot as plt
from skimage import data
from skimage import filters
from skimage import exposure
from skimage import io
from skimage.color import rgb2gray


camera = io.imread('IMG_7492.JPG')
camera = rgb2gray(camera)
#camera = data.camera()
val = filters.threshold_otsu(camera)

hist, bins_center = exposure.histogram(camera)

plt.figure(figsize=(9, 4))
plt.subplot(131)
plt.imshow(camera, cmap='gray', interpolation='nearest')
plt.axis('off')
plt.subplot(132)
plt.imshow(camera > val, cmap='gray', interpolation='nearest')
plt.axis('off')
plt.subplot(133)
plt.plot(bins_center, hist, lw=2)
plt.axvline(val, color='k', ls='--')

plt.tight_layout()
plt.show()


cam = (camera > val)*255
camera = io.imsave('IMG_7492-BW.JPG', cam)
