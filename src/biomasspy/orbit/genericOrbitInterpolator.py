#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 13:15:35 2018

@author: nunomiranda
"""

import numpy as np
from numpy.polynomial import Chebyshev
import scipy.constants as constants
from scipy import interpolate as interp
from scipy.optimize import root_scalar
from datetime import datetime
from dateutil import parser
from lxml import etree
import pathlib
from abc import ABC, abstractmethod
from ..utils import xpath_to_ndarray

inquiry = lambda x: print('INQUIRY:', x, x.size, x.shape, type(x))


class GenericOrbitInterpolator(ABC):
    """
    Abstracted class for the orbit interpolator
    """
    def __init__(self, position, velocity, time, orbit_number=None):
        """
        Generic constructor

        Parameter:
        ----------
        position : cartesian ECEF position ndarray [N, 3]
        velocity : cartesian ECEF velocity ndarray [N, 3]
        time : time ndarray [N] in the form of secs since a reference


        """

        try:
            if np.mod(position.size, 3) != 0:
                raise IndexError ("Inputs size shall be multiple of dimension [number_osv, 3] ")
        except:
            raise TypeError ("The input shall be a list or a numpy array of dimension [number_osv, 3] ")
        else:
            self._initialise(position, velocity, time, orbit_number=orbit_number)

    def __repr__(self):
        name = self.__class__.__name__
        domain = self.to_datetime64([self.svTime[0], self.svTime[-1]])
        dt = (domain[1] - domain[0]).astype('timedelta64[m]')

        return f"{name}(domain={domain}, nosv={self.number_state_vectors}, {str(dt)})"


    def __call__(self, xnew, acceleration = False):
        return self.interpolate(xnew, acceleration=acceleration)

    @property
    def number_state_vectors(self):
        return (self.nsv)

    def _initialise(self,position, velocity, time, orbit_number=None):
        """Initialise the main class variable"""
        self.nsv = len(position)
        self.svPos = np.array(position, dtype=np.float64)
        self.svVel = np.array(velocity, dtype=np.float64)
        self.svTime = np.array(time, dtype=np.float64)

        self.orbit_number = None
        if orbit_number is not None:
            assert (orbit_number.size == self.svTime.size), f"Orbit number shall be of same size as time "
            self.orbit_number = orbit_number

        self._position_interpolator = self._position_interpolator_init()
        self._velocity_interpolator = self._velocity_interpolator_init()
        self._acceleration_interpolator = self._acceleration_interpolator_init()

    def interpolate(self, xnew, acceleration = False):
        ''' performs orbit interpoaltion at the time xnew

            Parameters:
            ----------
            xnew : ndarray, shape (M)
                time at the desired interpolation

            acceleration : boolean, default is False
                computes and returns also acceleration as derivate of velocity

            returns:
            --------
            ynew : array , shape (M, 3)
                the satellite position  at the desired times

            y1new : array , shape (M, 3)
                the satellite velocity at the desired times

            y2new : array , shape (M, 3)
                the satellite acceleration at the desired times
        '''

        #check if is a scalar
        xnew = np.asarray(xnew)
        if np.ndim(xnew) == 0:
            xnew = xnew.reshape(1)

        if isinstance(xnew[0], (datetime, np.datetime64)):
            xnew = self.to_mjd2000(xnew)

        assert isinstance(xnew[0], type(self.svTime[0])), f"Time input not of the right type :  {type(xnew[0])} {type(self.svTime[0])}"
        #interpolate the position, velocity for the 3 axes
        ynew = np.zeros([xnew.size,3], dtype=np.float64)
        y1new = np.zeros([xnew.size,3], dtype=np.float64)
        y2new  = np.zeros([xnew.size,3], dtype=np.float64)

        ynew  = self._interpolate_position(xnew)
        y1new = self._interpolate_velocity(xnew)
        if acceleration == True:
            y2new = self._interpolate_acceleration(xnew)


        if xnew.size == 1:
            y1new= y1new.reshape(3)
            y2new= y2new.reshape(3)
            ynew = ynew.reshape(3)
        if acceleration ==True:
            return ynew,y1new,y2new
        else:
            return ynew, y1new

    @abstractmethod
    def _position_interpolator_init(self):
        """ The interpolator is let to each concrete class """
        raise NotImplementedError

    @abstractmethod
    def _interpolate_position(self, xnew):
        """Abstract method to interpolate the position

            Paramters:
            ----------
            xnew : ndarray, shape(M)
                time of the desired interpolation

            Returns:
            --------
            ynew : ndarray, shape (M,3)
                cartesian position at the desired time

        """
        raise NotImplementedError

    @staticmethod
    def to_mjd2000(timestamp, ref=np.datetime64('2000-01-01T00:00:00Z')):
        """
        """
        if hasattr(timestamp, '__iter__') == True:
            timestamp = np.asarray(timestamp)
            if isinstance(timestamp[0] , datetime):
                ref =  datetime(2000,1,1)
                time = [(t_-ref).total_seconds() for t_ in  timestamp]
                time = np.asarray(time)
            elif isinstance(timestamp[0] , np.datetime64):
                time = (timestamp - ref).astype('timedelta64[ns]')
                time = (np.double(time)* constants.nano).reshape(timestamp.shape)

        else:
            if isinstance(timestamp , datetime):
                time = (timestamp-ref).total_seconds()
            elif isinstance(timestamp , np.datetime64):
                time = (timestamp - ref).astype('timedelta64[ns]')
                time = np.double(time)* constants.nano

        return time

    @staticmethod
    def to_datetime64(mjd2000, ref=np.datetime64('2000-01-01T00:00:00Z')):
        """
        COnvert elaspsed seconds since the reference to datetime64

        Parameter
        ----------
        mjd2000:
            elapsed seconds since the reference
        Return
        -------
        datetime64

        """
        mjd2000 = np.atleast_1d(mjd2000)
        dt = (mjd2000*1e9).astype(np.int)
        dt = dt.astype('timedelta64[ns]')
        dt = dt.squeeze()
        return ref + dt


    @abstractmethod
    def _velocity_interpolator_init(self):
        """ The interpolator is let to each concrete class """
        raise NotImplementedError

    @abstractmethod
    def _interpolate_velocity(self, xnew):
        """Abstract method to interpolate the velocity

            Paramters:
            ----------
            xnew : ndarray, shape(M)
                time of the desired interpolation

            Returns:
            --------
            ynew : ndarray, shape (M,3)
                cartesian velocity at the desired time

        """
        raise NotImplementedError

    @abstractmethod
    def _acceleration_interpolator_init(self):
        """ The interpolator is let to each concrete class """
        raise NotImplementedError

    @abstractmethod
    def _interpolate_acceleration(self, xnew):
        """
        Interpolates the satelitte acceleration using the defined interpolator

        Parameters:
        -----------
        xnew : ndarray, shape (M)
            array of time_string_to_double

        returns:
        --------
        ynew : array , shape (M, 3)
            the acceleration velocity  at the desired times
        """
        raise NotImplementedError


    def __del__(self):
        pass #do nothing

    def __exit__(self, exc_type, exc_value, traceback):
        pass


    def find_anx(self):
        """Find the ANX times if there is an ANX crossibg"""
        f = self._position_interpolator[2]
        anx_cross_idx = np.where(np.diff(np.sign(self.svPos[:, 2])) > 0)[0]
        orbit_list = np.unique(self.orbit_number)
        anx_time = []
        if anx_cross_idx.size > 0:
            for idx in anx_cross_idx:
                a, b = self.svTime[idx], self.svTime[idx + 1]
                x0 = self.svPos[idx, 2],
                x1 = self.svPos[idx + 1, 2]

                #print (idx, self.to_datetime64((a, b)))
                if np.abs(x0) < 1e-6:
                    anx_time.append(a)
                    #print("A", a, anx_time, self.to_datetime64(a))
                elif np.abs(x1) < 1e-6:
                    anx_time.append(b)
                    #print("B", b)
                else:
                    sol = root_scalar(f, method='brentq', bracket=(a,b),
                                  x0=x0,
                                  x1=x1)
                    #print("rootsol.root)
                    anx_time.append(sol.root)

        anx_time = np.asarray(anx_time)
        # todo : the computation of the orbit number needs to be done with more care
        # this a quick fix to make it work
        orbit_number = self.orbit_number[anx_cross_idx[0]] + np.arange(anx_cross_idx.size)

        return dict(zip(orbit_number.astype(np.int), anx_time))

       # return orbit_number.astype(np.int), anx_time




    @classmethod
    def read_esa_orbit(cls, esa_orbit_eof, **kwargs):
        """ Method loading an ESA orbit file following the EOF formar
        """

        if isinstance(esa_orbit_eof, pathlib.Path):
            esa_orbit_eof_str = str(esa_orbit_eof.absolute())
        else : esa_orbit_eof_str = esa_orbit_eof

        root = etree.parse(esa_orbit_eof_str).getroot()
        xp ='Data_Block/List_of_OSVs'
        Nosv = np.int(root.find(xp, root.nsmap).attrib['count'])

        p,v =  np.zeros((Nosv,3)), np.zeros((Nosv,3))
        for ix, dim in enumerate(['X','Y','Z']):
            pos,  vel = f".//{dim}",  f".//V{dim}"
            p[:,ix] = xpath_to_ndarray(root, pos, cast_type=np.double, namespace=root.nsmap)
            v[:,ix] = xpath_to_ndarray(root, vel, cast_type=np.double, namespace=root.nsmap)

        time = xpath_to_ndarray(root, './/UTC')
        time = [ t_.replace('UTC=','') for t_ in time ]
        time = [ parser.parse(t_) for t_ in time ]
        time = cls.to_mjd2000(time)

        xp = f'.//Absolute_Orbit'
        orbit_number = xpath_to_ndarray(root, xp, cast_type=np.double, namespace=root.nsmap)
        orb = cls(p, v, time, orbit_number=orbit_number, **kwargs)

        return orb



class PolynomialOrbitInterpolator(GenericOrbitInterpolator):
    """
    Concrete class implementing the orbit interpolation based on
    Chebyshev polynomial interpolator
    """
    def __init__(self, position, velocity, time, orbit_number=None, deg=4):
        """
        Generic constructor

        Parameter:
        ----------
        position : cartesian ECEF position ndarray [N, 3]
        velocity : cartesian ECEF velocity ndarray [N, 3]
        time : time ndarray [N] in the form of secs since a reference
        deg : degree of the polynomial interpolator

        """

        self.deg = deg
        super().__init__(position, velocity, time, orbit_number=orbit_number)



    def _position_interpolator_init(self):
        """ Initialise the Spline looping on all the 3 dimensions for the position

        Return:
        ------
        dum : list [3]
            list of 3 polynomials for the X,Y,Z dimension
        """
        dum = [ Chebyshev.fit(self.svTime, self.svPos[:,i], self.deg) for i in range(3)]
        return dum

    def _interpolate_position(self, xnew):
        """
        Interpolates the satelitte position using the defined _position_interpolator

        Parameters:
        -----------
        xnew : ndarray, shape (M)
            array of double

        returns:
        --------
        ynew : array , shape (M, 3)
            the satellite position  at the desired times
        """
        ynew = np.zeros([xnew.size,3], dtype=np.float64)
        for i in range(0,3):
            ynew[:,i]  = self._position_interpolator[i](xnew)

        return ynew


    def _velocity_interpolator_init(self):
        """ Initialise the Spline looping on all the 3 dimensions for veloicty

        Return:
        ------
        dum : list [3]
            list of 3 Splines for the X,Y,Z dimension
        """
        dum = [ Chebyshev.fit(self.svTime, self.svVel[:,i], self.deg) for i in range(3)]
        return dum


    def _interpolate_velocity(self, xnew):
        """
        Interpolates the satelitte velocity using the defined interpolator

        Parameters:
        -----------
        xnew : ndarray, shape (M)
            array of time_string_to_double

        returns:
        --------
        ynew : array , shape (M, 3)
            the satellite velocity  at the desired times
        """
        ynew = np.zeros([xnew.size,3], dtype=np.float64)
        for i in range(0,3):
            ynew[:, i]  = self._velocity_interpolator[i](xnew)
        return ynew



    def _acceleration_interpolator_init(self):
        """
        Returns the satellite acceleration interpolator

        Return:
        ------
        dum : list [3]
            list of 3 derivate splines
        """
        dum = [self._velocity_interpolator[i].deriv(1) for i in range(3)]
        return dum

    def _interpolate_acceleration(self, xnew):
        """
        Interpolates the satelitte acceleration using the defined interpolator

        Parameters:
        -----------
        xnew : ndarray, shape (M)
            array of time_string_to_double

        returns:
        --------
        ynew : array , shape (M, 3)
            the acceleration  at the desired times
        """
        ynew = np.zeros([xnew.size,3], dtype=np.float64)
        for i in range(0,3):
            ynew[:, i] = self._acceleration_interpolator[i](xnew)
        return ynew


class SplineOrbitInterpolator(GenericOrbitInterpolator):
    """
    Concrete class implementing the orbit interpolationed based on
    Univariated Splines
    """


    def _position_interpolator_init(self):
        """ Initialise the Spline looping on all the 3 dimensions for the position

        Return:
        ------
        dum : list [3]
            list of 3 Splines for the X,Y,Z dimension
        """
        dum = [ interp.InterpolatedUnivariateSpline(self.svTime, self.svPos[:,i]) for i in range(3)]
        return(dum)

    def _interpolate_position(self, xnew):
        """
        Interpolates the satelitte position using the defined _position_interpolator

        Parameters:
        -----------
        xnew : ndarray, shape (M)
            array of time_string_to_double

        returns:
        --------
        ynew : array , shape (M, 3)
            the satellite position  at the desired times
        """
        ynew = np.zeros([xnew.size,3], dtype=np.float64)
        for i in range(0,3):
            ynew[:,i]  = self._position_interpolator[i](xnew)

        return ynew


    def _velocity_interpolator_init(self):
        """ Initialise the Spline looping on all the 3 dimensions for veloicty

        Return:
        ------
        dum : list [3]
            list of 3 Splines for the X,Y,Z dimension
        """
        dum = [ interp.InterpolatedUnivariateSpline(self.svTime, self.svVel[:,i]) for i in range(3)]
        return(dum)


    def _interpolate_velocity(self, xnew):
        """
        Interpolates the satelitte velocity using the defined interpolator

        Parameters:
        -----------
        xnew : ndarray, shape (M)
            array of time_string_to_double

        returns:
        --------
        ynew : array , shape (M, 3)
            the satellite velocity  at the desired times
        """
        ynew = np.zeros([xnew.size,3], dtype=np.float64)
        for i in range(0,3):
            ynew[:,i]  = self._velocity_interpolator[i](xnew)
        return ynew



    def _acceleration_interpolator_init(self):
        """
        Returns the satellite acceleration interpolator

        Return:
        ------
        dum : list [3]
            list of 3 derivate splines
        """
        dum = [  spl.derivative() for spl in self._velocity_interpolator ]
        return dum

    def _interpolate_acceleration(self, xnew):
        """
        Interpolates the satelitte acceleration using the defined interpolator

        Parameters:
        -----------
        xnew : ndarray, shape (M)
            array of time_string_to_double

        returns:
        --------
        ynew : array , shape (M, 3)
            the acceleration  at the desired times
        """
        ynew = np.zeros([xnew.size,3], dtype=np.float64)
        for i in range(0,3):
            ynew[:,i]  = self._acceleration_interpolator[i](xnew)
        return ynew


