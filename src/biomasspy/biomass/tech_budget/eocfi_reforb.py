import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt

from dataclasses import dataclass, asdict, field
from typing import Union, List, Callable, Optional, Any
    
from xml.etree import ElementTree
from datetime import datetime

from ...orbit.genericOrbitInterpolator import SplineOrbitInterpolator as OrbitInterpolator


repeat_cycle_length = 44


NUMBER_INSAR_PASS = {'INT' :3, 'TOM' : 7}

MAJOR_CYCLE_TEMPLATE = {'INT' : [3,3,3,3],
                        'TOM' : [7,7,7,3]}





@dataclass
class OSV:
    orbit_number : int = 0
    utc : Any =0 
    p : Any =0
    v : Any =0
    
        
    @classmethod
    def from_record(cls, rec):
        return cls(orbit_number=rec.orbit, utc=rec.ANX, p=rec.POS, v=rec.VEL )
        
    def to_xml(self):
        dt = np.timedelta64(36, 's')
        at = self.utc+dt
        
        osv_rec = ElementTree.Element('OSV')
        tai = ElementTree.Element('TAI')
        tai.text = f"TAI={at.isoformat()}"
        
        utc = ElementTree.Element('UTC')
        utc.text = f"UTC={self.utc.isoformat()}"
        ut1 = ElementTree.Element('UT1')
        ut1.text = f"UT1={self.utc.isoformat()}"
        orb = ElementTree.Element('Absolute_Orbit')
        orb.text = f"{self.orbit_number:+06d}"
        x = ElementTree.Element('X',attrib={'unit':'m'})
        x.text = f"{self.p[0]:+012.3f}"
        y = ElementTree.Element('Y',attrib={'unit':'m'})
        y.text = f"{self.p[1]:+012.3f}"
        z = ElementTree.Element('Z',attrib={'unit':'m'})
        z.text = f"{self.p[2]:+012.3f}"
        vx = ElementTree.Element('VX',attrib={'unit':'m/s'})
        vx.text = f"{self.v[0]:+012.6f}"
        vy = ElementTree.Element('VY',attrib={'unit':'m/s'})
        vy.text = f"{self.v[1]:+012.6f}"
        vz = ElementTree.Element('VZ',attrib={'unit':'m/s'})
        vz.text = f"{self.v[2]:+012.6f}"
        q= ElementTree.Element('Quality')
        q.text = f"00000000000000"
        
        for rec_ in [tai, utc, ut1, orb, x, y, z, vx, vy, vz, q]:
            osv_rec.append(rec_)
        return osv_rec
    
    def utc_s(self):
        return f"UTC={self.utc}"
        
        
class OrbitScenario:
    def __init__(self,orbit_file_list, phase='INT'):
        self.orbit_file_list = sorted(orbit_file_list)
        self.phase = phase
        self.scenario = None
        
        self.__post_init__()
    
    def __post_init__(self):
        scenario = np.asarray([])
        # put all the orbit files in a data frame
        for ix, orb_file in enumerate(self.orbit_file_list):
            orb_ = OrbitInterpolator.read_esa_orbit(orb_file)
            
            anx_time = orb_.find_anx()
            orbital_period = np.diff(list(anx_time.values()))
            
            orbits = np.unique(orb_.orbit_number)
            for orbit_number_, anx_ in anx_time.items():
                p,v = orb_(anx_)
                #print (orbit_number_, orb_.to_datetime64(anx_)  )
                scenario = np.append(scenario, [ix, orbit_number_, orb_.to_datetime64(anx_), p, v], axis=0)

        scenario = scenario.reshape(-1,5)
        self.scenario = pd.DataFrame(data=scenario,columns=[f"orb_file", "orbit", f"ANX", 'POS', 'VEL'])
        self.scenario.sort_values('orbit',inplace=True, ignore_index=True)
        
        self._scenario =  self.scenario
        #remove duplicates
        self.scenario.drop_duplicates(subset='orbit', keep='last', inplace=True, ignore_index=True)
        self.scenario['REPEAT_CYCLE'] = self.repeat_cycle
        self.scenario['MAJOR_CYCLE'] = self.major_cycle
        self.scenario['SWATH_CYCLE'] = self.swath_cycle

        
    @property
    def orbital_period(self):
        anx =  self.scenario.ANX.values
        return np.diff(anx)/1e9
    
    
    @property 
    def orbit_number(self):
        """ Orbit number is 1-based
        """
        return self.scenario.orbit.values
    
    @property
    def repeat_cycle(self):
        """ Repeat cycle is also 1-based
            orbit [0 ...43] -> 0+1
            orbit [44 ...87] -> 1+1
            
        """
        rc = (self.orbit_number-1)  // repeat_cycle_length
        rc+=1
        return rc
    @property
    def major_cycle(self):
        """ Major cycle is also 1-based
              
        """
        number_repeat_pass =  MAJOR_CYCLE_TEMPLATE[self.phase]
        mc_lenght = np.asarray(number_repeat_pass).sum()
        return (self.orbit_number-1) // (mc_lenght * repeat_cycle_length) +1
    
            
    @property
    def swath_cycle(self):
        number_insar_pass = NUMBER_INSAR_PASS[self.phase]
        major_cycle_template =  np.asarray(MAJOR_CYCLE_TEMPLATE[self.phase])
        swath_cycle = np.zeros(self.orbit_number.size)
        
        major_cycle_list = np.unique(self.major_cycle)
        for mc in major_cycle_list:
            mc_ix = np.where(self.major_cycle == mc)[0]
            
            # 1-based repeat cycle
            rc1 = self.repeat_cycle[mc_ix]
            rc = np.arange(major_cycle_template.sum()*repeat_cycle_length) //repeat_cycle_length

            swath_cycle[mc_ix] =  rc // number_insar_pass % len(major_cycle_template) +1
        return swath_cycle
    

    
    def check(self):
        number_insar_pass = NUMBER_INSAR_PASS[self.phase]
        major_cycle_template =  MAJOR_CYCLE_TEMPLATE[self.phase]
        
        orbit = self.scenario.orbit.values
        anx =  self.scenario.ANX.values
        

        #orbital period
        t_orb = np.diff(anx)/1e9

        plt.style.use('ggplot')
        fig, ax = plt.subplots(nrows=4, figsize = (20,10),)

        ax[0].plot(orbit)
        ax[0].set_ylabel('orbit number')
        
        ax[1].plot(self.major_cycle)
        
        ax[1].plot(self.swath_cycle)
        ax[1].plot(t_orb-t_orb[0], marker='o')

        ax[1].set_ylabel('major cycle')
        
        ax[2].plot(np.diff(orbit), marker='o')
        ax[2].set_ylabel('orbit number diff')
        
        ax[3].plot(t_orb, marker='o')
        ax[3].set_ylabel('orbital period [s]')
        
    
    def write_csv(self, out_csv):
        self.scenario.to_csv(out_csv)
        
    def write_xml(self, out_xml, template='Biomass_RefOrb_INT_MC2_RC3_S3.EEF'  ):
        with open(template, "r") as file:
            tree = ElementTree.parse(file)
            root = tree.getroot()
            
            #First remove all the childs
            list_osvs = root.find('./Data_Block/List_of_OSVs')
            for ix, child_ in enumerate(list_osvs.getchildren()):
                    list_osvs.remove(child_)

            #Second add new childs
            for ix, rec in self.scenario.iterrows():
                eocfi_osv = OSV.from_record(rec)
                if ix ==0:
                    first_eocfi_osv = eocfi_osv 
                list_osvs.append(eocfi_osv.to_xml())
            #update count
            list_osvs.attrib['count'] = str(len( self.scenario))

            #tune the header
            val_start = root.find('./Earth_Explorer_Header/Fixed_Header/Validity_Period/Validity_Start')
            val_stop = root.find('./Earth_Explorer_Header/Fixed_Header/Validity_Period/Validity_Stop')
            creation_date = root.find('./Earth_Explorer_Header/Fixed_Header/Source/Creation_Date')
            creator = root.find('./Earth_Explorer_Header/Fixed_Header/Source/Creator')
            note = root.find('./Earth_Explorer_Header/Fixed_Header/Notes')
            val_stop.text = eocfi_osv.utc_s()
            val_start.text = first_eocfi_osv.utc_s()
            creation_date.text = f"UTC={datetime.now()}"
            creator.text = f"BIOMASS PDGS"
            ll = [i.stem for i in self.orbit_file_list ]
            note.text = 'Assembly of\n\t\t '+'\n\t\t'.join(ll)


            # create a new XML file with the results
            mydata = ElementTree.tostring(root)
            with open(out_xml, "wb") as myfile:
                myfile.write(mydata)