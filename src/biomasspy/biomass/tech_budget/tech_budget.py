from typing import Union, List
from pathlib import Path
import pandas as pd
import geopandas as gpd
import numpy as np
from scipy.constants import minute, hour, day
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from shapely.geometry import Polygon

from ..isp.ispLogger import get_logger

WORLD = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))

logger = get_logger(__name__)

KILO = 1024.
MEGA = KILO ** 2
GIGA = MEGA * KILO
TERA = GIGA * KILO
BITS = 8
NUMBER_RX_CHANNEL = 2

# SAR DATA ICD
SWATH_LIST = ['S1', 'S2', 'S3', 'SRM']
PRF = 3000

# data rate in mega bits for the two channels
SAR_BAQ5_DATA_RATE = dict(zip(SWATH_LIST, np.asarray([77.75897371, 97.19871713, 116.63846056, 116.63846056])))

# SAR DATA ICD, issue 4, from the Fig 6-1 for 1 Rx channel
SAR_ANC_DATA_RATE = 170 * BITS / MEGA

# S2G ICD, section 8.1, rate = 1Hz , 1 packet = 304 Bytes
# BEEPS is however coding as 306
PLAT_ANC_DATA_RATE = 306 * BITS / MEGA
PLAT_ANC_DATA_BUFFER = 16  # seconds

# S2G ICD, section 6.4.2
CADU_SIZE_BYTE = 2044  # bytes
TF_SIZE_BYTE = 1912  # bytes
# MAR Mega bits/s
CADU_DATA_RATE = 132.4

DATATAKE_DURATION_MIN = 32  # seconds

__all__ = ['TechBudget', 'CaduTechBudget', 'Level0ScienceTechBudget', 'InstrAncTechBudget', 'Level1StaTechBudget']


class TechBudget:
    def __init__(self, savoir_export: pd.DataFrame):
        self.simulation = savoir_export
        self.simulation.sort_values(by='Start', inplace=True)
        if self.simulation.Duration.min() < DATATAKE_DURATION_MIN:
            logger.warning(f"Simulation has duration < {DATATAKE_DURATION_MIN} s")
        self._geometry_init()

    def __repr__(self):
        name = self.__class__.__name__
        return f"{name} (overpasses={len(self.simulation)})"

    @classmethod
    def from_savoir(cls, savoir_export: Union[str, Path, pd.DataFrame]):
        """
        Create a tech budget from a savoir export
        """

        if isinstance(savoir_export, (str, Path)):
            savoir_export = pd.read_csv(savoir_export, parse_dates=['Start', 'End', 'Anx'], )

        return cls(savoir_export)

    def patch_mission_timeline(self, mission_timeline):
        """
        Add annotation related to the major cycle repeat cycle and swath cycle
        """
        df = pd.read_csv(mission_timeline)
        self.simulation['REPEAT_CYCLE'] = 0
        self.simulation['SWATH_CYCLE'] = 0
        self.simulation['MAJOR_CYCLE'] = 0
        self.simulation['GLOBAL_COVERAGE'] = 1
        for ii, rec in df.iterrows():
            orbit = rec.orbit
            ix = self.simulation.Orbit == orbit
            self.simulation.loc[ix, 'MAJOR_CYCLE'] = rec.MAJOR_CYCLE
            self.simulation.loc[ix, 'REPEAT_CYCLE'] = rec.REPEAT_CYCLE
            self.simulation.loc[ix, 'SWATH_CYCLE'] = rec.SWATH_CYCLE

    def _geometry_init(self):
        """
        Transform the lat/lons provided by savoir into a geometry elligible to geopandas
        """
        if 'geometry' not in self.simulation:
            geometries = []
            for ix, rec in self.simulation.iterrows():
                lats = [rec.NW_Lat, rec.NE_Lat, rec.SE_Lat, rec.SW_Lat, rec.NW_Lat]
                lons = [rec.NW_Lon, rec.NE_Lon, rec.SE_Lon, rec.SW_Lon, rec.NW_Lon]
                geometries.append(Polygon(list(zip(lons, lats))))
            self.simulation = gpd.GeoDataFrame(self.simulation, geometry=geometries)

    @property
    def number_packets(self):
        """
        Compute the number of source packets during the sensing
        """
        duration = self.simulation['Duration'].values
        num_packets = duration * PRF
        return num_packets

    @staticmethod
    def display_geometry(df, extent=None, column='SensorMode', figsize=(10, 6)):
        if 'geometry' not in df:
            geometries = []
            for ix, rec in df.iterrows():
                lats = [rec.NW_Lat, rec.NE_Lat, rec.SE_Lat, rec.SW_Lat, rec.NW_Lat]
                lons = [rec.NW_Lon, rec.NE_Lon, rec.SE_Lon, rec.SW_Lon, rec.NW_Lon]
                geometries.append(Polygon(list(zip(lons, lats))))
            df = gpd.GeoDataFrame(df, geometry=geometries)

        plt.style.use('default')
        fig, ax = plt.subplots(figsize=figsize)
        ax = plt.axes(projection=ccrs.PlateCarree())

        ax.coastlines()

        df.plot(ax=ax, linewidth=3, column=column, legend=True)
        if extent is not None:
            ax.set_extent(extent)
        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5,
                          linestyle='--')
        return df


    def _stats(self, parameter,
               groupers: List = ['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE', 'MAJOR_CYCLE',
                                 'GLOBAL_COVERAGE'],
               parameter_scaling: List = [1, 1, 1, 1, 1, 1],
               out_column_name: List = ['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle', 'Global Coverage'],
               out_column_unit: List = ['.', '.', '.', '.', '.', '.']):

        """
        Computing the overall sensing statistics by grouping the in different manner.
        The parameters is grouped sequentially by each grouper elements.
        For each grouped view, the first order statistics are computed and appended to same data frame

        Parameters:
        ----------
            parameter : str
                teh name of the column analysed e.g. Duration or Volume or number of Products
            groupers : list of groupers
                a grouper can be a string representing simulation dataframe column name or a pd.Grouper
            parameter_scaling : List of numbers
                the parameter is divided by this number. Allows to convert secs -> min -> Hour our Bytes -> MB -> ...
            out_column_name :str
                Verbose Name of the putput colums
            out_column_unit : str
                Unit appended to the out colum name

        Return:
        -------
            pd.DataFrame
                Grouped fird order statistics

        """
        pd.options.display.float_format = "{: .2f}".format
        simulation = self.simulation

        df = None
        time_scale = parameter_scaling
        suffix = out_column_unit
        col_name = out_column_name
        col_title = []

        for sc, grp, suf, col in zip(time_scale, groupers, suffix, col_name):
            #logger.info(f"Grouping of {parameter} by {col}")
            elt = (simulation.groupby(by=grp).sum()[[parameter]] / sc).describe()
            # adding as new column
            if df is None:
                df = elt
            else:
                df[grp] = elt

            col_title.append(f"{col} [{suf}]")

        mapper = dict(zip(df.columns, col_title))

        df.rename(columns=mapper, inplace=True)
        return df

    def sensing_stats(self, title=None):
        df = self._stats('Duration',
                         groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE', 'MAJOR_CYCLE',
                                   'GLOBAL_COVERAGE'],
                         parameter_scaling=[1, minute, minute, hour, hour, hour],
                         out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                          'Global Coverage'],
                         out_column_unit=['s', 'M', 'M', 'H', 'H', 'H'])
        if title is None:
            title = 'BIOMASS Sensing Statistics'
        return df.style.set_caption(title)


class CaduTechBudget(TechBudget):
    def __init__(self, savoir_export):
        super().__init__(savoir_export)
        self._cadu_init()

        logger.info(f"CADU data rate used is {self.data_rate()} Mbits/s")

    def _cadu_init(self):
        """
        Compute the volume associated to CADU in **MByte**
        Volume  = sensing [s] * data_rate Mbits/s
        """
        duration = self.simulation['Duration'].values
        self.simulation['CADU_VOLUME_MB'] = np.round(duration * self.data_rate() / BITS).astype(int)

    def data_rate(self):
        """Compute the data rate for the class in Mbits / s
        The data rate is given for the two RX channels

        THE CADU rate is the one defined in the MAR and represents a 11% increase
        with respect to the SAR worst case bit rate

        """
        return CADU_DATA_RATE

    def volume_stats(self, title=None):
        df = self._stats('CADU_VOLUME_MB',
                         groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                   'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                         parameter_scaling=[1024 / 8, 1024 / 8, 1024 / 8, 1024 / 8, 1024 * 1024, 1024 * 1024],
                         out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                          'Global Coverage'],
                         out_column_unit=['Gb', 'Gb', 'Gb', 'Gb', 'TB', 'TB'])
        if title is None:
            title = 'BIOMASS Downlinked (CADU) Volume Statistics'
        return df.style.set_caption(title)


class InstrAncTechBudget(TechBudget):
    def __init__(self, savoir_export):
        super().__init__(savoir_export)
        self._init()

        logger.info(f"Instrument Ancillary data rate used is {self.data_rate()} Mbits/s")

    def _init(self):
        """
        Compute the volume associated to CADU in **MByte**
        Volume  = sensing [s] * data_rate Mbits/s
        """

        self.simulation['SARANC_VOLUME_MB'] = self.duration.values * self.data_rate() / BITS

    @property
    def duration(self):
        return self.simulation[['Duration']]

    def data_rate(self):
        """Compute the data rate for the class in Mbits / s
        The data rate is given for the two RX channels

        THE  rate is the defined from the SAR DATA ICD uing the packert size and th 1Hz rate
        It needs to be multiplied by the  NUMBER_RX_CHANNEL
        """
        return SAR_ANC_DATA_RATE * NUMBER_RX_CHANNEL

    def test(self):
        duration = 78
        volume = duration * self.data_rate() / BITS

        volume_byte = volume * MEGA
        print(f"{duration}[s] *  {self.data_rate()} Mbps = volume [bits]  = {volume_byte}[B]")
        assert (volume_byte == 13260.0 * 2)

    def volume_stats(self, title=None):
        df = self._stats('SARANC_VOLUME_MB',
                         groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                   'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                         parameter_scaling=[1 / BITS, 1, 1, 1, 1, 1024],
                         out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                          'Global Coverage'],
                         out_column_unit=['Mb', 'Mb', 'MB', 'MB', 'MB', 'GB'])
        if title is None:
            title = 'BIOMASS Downlinked (Instrument Ancillary) Volume Statistics'
        return df.style.set_caption(title)


class PlatAncTechBudget(TechBudget):
    def __init__(self, savoir_export):
        super().__init__(savoir_export)
        self._init()

        logger.info(f"Platform Ancillary data rate used is {self.data_rate()} Mbits/s")

    def _init(self):
        """
        Compute the volume associated to CADU in **MByte**
        Volume  = sensing [s] * data_rate Mbits/s
        """

        self.simulation['PLATANC_VOLUME_MB'] = self.duration.values * self.data_rate() / BITS

    @property
    def duration(self):
        return self.simulation[['Duration']] + PLAT_ANC_DATA_BUFFER

    def data_rate(self):
        """Compute the data rate for the class in Mbits / s
        The data rate is given for the two RX channels

        THE CADU rate is the one defined in the MAR and represents a 11% increase
        with respect to the SAR worst case bit rate

        """
        return PLAT_ANC_DATA_RATE

    def test(self):
        duration = 78
        volume = duration * self.data_rate() / BITS

        volume_byte = volume * MEGA
        print(f"{duration}[s] *  {self.data_rate()} Mbps = volume [bits]  = {volume_byte}[B]")
        assert (volume_byte == 23868.0), f"{volume_byte} != {13260.0}"


    def volume_stats(self, title=None):
        df = self._stats('PLATANC_VOLUME_MB',
                         groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                   'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                         parameter_scaling=[1 / BITS, 1, 1, 1, 1, 1024],
                         out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                          'Global Coverage'],
                         out_column_unit=['Mb', 'Mb', 'MB', 'MB', 'MB', 'GB'])
        if title is None:
            title = 'BIOMASS Downlinked (Platform Ancillary) Volume Statistics'
        return df.style.set_caption(title)


class Level0ScienceTechBudget(TechBudget):
    # Production model
    l0_slice_duration = 114
    l0_slice_overlap = 9

    def __init__(self, savoir_export_file):
        super().__init__(savoir_export_file)
        self._init()

    def _init(self):
        """
        Compute tech budget elements for the L0 products
        Explicit formulae here
        """

        self.simulation['L0_NUMBER_SLICES'] = self.number_slices
        self.simulation['L0_VOLUME_MB'] = np.round(self.slice_overall_volume / BITS).astype(int)

    @property
    def number_slices(self):
        """
        Number of L0 slices in the data-take
        """
        return np.ceil(self.simulation['Duration'].values / self.l0_slice_duration).astype(int)

    @property
    def slice_overall_duration(self):
        number_full_slices = self.simulation['Duration'].values // self.l0_slice_duration
        last_slice_duration = self.simulation['Duration'].values - number_full_slices * self.l0_slice_duration
        return number_full_slices * (self.l0_slice_duration + self.l0_slice_overlap) + (
                    last_slice_duration + self.l0_slice_overlap)

    @property
    def slice_overall_volume(self):
        return self.slice_overall_duration * self.data_rate()

    def data_rate(self, swath=None):
        """Compute the data rate for the class in Mbits / s
        The data rate is given for the two RX channels

        if swath is passed then data rate for this swath is retuned
        if not, the mean bit rate is returned

        Parameters:
        -----------
            swath: str in ['S1', 'S2', 'S3', 'SRM']
        Returns:
        ------
            float:
                data rate in Mbits/s

        """
        # Mbits/s for two channels
        max_sar_hbr_rate = np.max(list(SAR_BAQ5_DATA_RATE.values()))
        return max_sar_hbr_rate


    def volume_stats(self, title=None):
        df= self._stats('L0_VOLUME_MB',
                           groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                     'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                           parameter_scaling=[1024 / 8, 1024 / 8, 1024 / 8, 1024 / 8, 1024 * 1024, 1024 * 1024],
                           out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                            'Global Coverage'],
                           out_column_unit=['Gb', 'Gb', 'Gb', 'Gb', 'TB', 'TB'])

        if title is None:
            title = 'BIOMASS L0 Volume Statistics (including slice overlap)'
        return df.style.set_caption(title)

    def slice_stats(self, title=None):
        df= self._stats('L0_NUMBER_SLICES',
                           groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                     'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                           parameter_scaling=[1, 1, 1, 1, 1, 1],
                           out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                            'Global Coverage'],
                           out_column_unit=['#', '#', '#', '#', '#', '#'])
        if title is None:
            title = 'BIOMASS L0 Slice number Statistics'
        return df.style.set_caption(title)


class Level0MonTechBudget(TechBudget):
    # RDB issue 9, values in seconds
    gpre_duration = 0.03
    gpos_duration = 0.03
    gcal_duration = 0.02
    time_slot_duration = 10

    def __init__(self, savoir_export_file):
        super().__init__(savoir_export_file)
        self._init()

    def _init(self):
        """
        Compute tech budget elements for the L0 products
        Explicit formulae here
        """

        self.simulation['L0M_VOLUME_MB'] = self.volume / BITS

    @property
    def duration(self):
        num_time_slots = self.simulation['Duration'].values // self.time_slot_duration
        duration = self.gpre_duration + self.gpos_duration + num_time_slots * self.gcal_duration
        return duration

    @property
    def volume(self):
        """Volume in Mega bits"""
        return self.duration * self.data_rate()

    def data_rate(self, ):
        """Data rate for GCAL, GPR and GPOS.
        The data rate for the class in Mbits / s
        The data rate is given for the two RX channels

        The Data rate is assumed as two times the sar dara datre  due to the fact that the
        internal calibration pulses are in BAQ bypassed
        At least 10 bits are used
        Returns:
        ------
            float:
                data rate in Mbits/s

        """
        # Mbits/s for two channels
        max_sar_hbr_rate = np.max(list(SAR_BAQ5_DATA_RATE.values()))
        # the x2 represnts the fact that data will be bypassed so at least 10 bits will be used
        #
        return max_sar_hbr_rate * 2


    def volume_stats(self):
        return self._stats('L0M_VOLUME_MB',
                           parameter_scaling=[1 / BITS, 1, 1, 1, 1, 1024],
                           out_column_unit=['Mb', 'Mb', 'MB', 'MB', 'MB', 'GB'])

    def slice_stats(self):
        return self._stats('L0_NUMBER_SLICES',
                           parameter_scaling=[1, 1, 1, 1, 1, 1],
                           out_column_unit=['#', '#', '#', '#', '#', '#'])


class AuxOrbitBudget(PlatAncTechBudget):
    header_size = 1721 * BITS  # bits
    osv_xml_size = 498 * BITS  # bits

    def __init__(self, savoir_export):
        super().__init__(savoir_export)
        self.simulation['ORBIT_VOLUME_MB'] = self.volume()

        logger.info(f"Platform AuxOrbit data rate used is {self.data_rate()} Mbits/s")

    def volume(self):
        """
        Volume in MByte
        """
        osv_bits = self.duration * self.data_rate() * MEGA
        vol = (self.header_size + osv_bits) / BITS / MEGA
        return vol

    def data_rate(self):
        """
        Orbits state vectors are provided at 1Hz
        data rate in Mbits / s
        """
        return self.osv_xml_size / MEGA

    def test(self):
        duration = 4615
        osv_bits = duration * self.data_rate() * MEGA
        vol = (self.header_size + osv_bits) / BITS / MEGA
        assert (vol == 2.1934423446655273)

    def volume_stats(self, title=None):
        df= self._stats('ORBIT_VOLUME_MB',
                           groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                     'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                           parameter_scaling=[1024, 1024 , 1024, 1024 , 1024 * 1024, 1024 * 1024],
                           out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                            'Global Coverage'],
                           out_column_unit=['GB', 'GB', 'GB', 'GB', 'TB', 'TB'])
        if title is None:
            title = 'BIOMASS Orbit (EOCFI) file volume statistics'
        return df.style.set_caption(title)


class AuxAttitudeBudget(PlatAncTechBudget):
    header_size = 1721 * BITS  # bits
    osv_xml_size = 234 * BITS  # bits

    def __init__(self, savoir_export):
        super().__init__(savoir_export)
        self.simulation['ATTITUDE_VOLUME_MB'] = self.volume()
        logger.info(f"Platform AuxAttitude data rate used is {self.data_rate()} Mbits/s")

    def volume(self):
        """
        Volume in MByte
        """
        osv_bits = self.duration * self.data_rate() * MEGA
        vol = (self.header_size + osv_bits) / BITS / MEGA
        return vol

    def data_rate(self):
        """
        Orbits state vectors are provided at 1Hz
        data rate in Mbits / s
        """
        return self.osv_xml_size / MEGA

    def test(self):
        duration = 4615
        osv_bits = duration * self.data_rate() * MEGA
        vol = (self.header_size + osv_bits) / BITS / MEGA
        assert (vol == 2.1934423446655273)

    def volume_stats(self, title=None):
        df= self._stats('ATTITUDE_VOLUME_MB',
                           groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                     'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                           parameter_scaling=[1024, 1024 , 1024, 1024 , 1024 * 1024, 1024 * 1024],
                           out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                            'Global Coverage'],
                           out_column_unit=['GB', 'GB', 'GB', 'GB', 'TB', 'TB'])
        if title is None:
            title = 'BIOMASS Attitude (EOCFI) file volume statistics'
        return df.style.set_caption(title)


class Level1SlcTechBudget(TechBudget):
    # Production model
    frame_duration = 19
    frame_overlap = 2
    prf = PRF / 2
    number_channel = NUMBER_RX_CHANNEL * 2
    complex_sample = 2
    mean_number_samples = 1560 #worst case SW3
    bits_sample = 4 * BITS #1 float = 4 bytes = 4 * bITS
    tiff_compression_ratio = 2

    def __init__(self, savoir_export_file):
        super().__init__(savoir_export_file)
        self._init()

    def _init(self):
        """
        Compute tech budget elements for the L0 products
        Explicit formulae here
        """

        self.simulation['L1_FRAME_NUMBER'] = self.number_frames
        self.simulation['L1A_VOLUME_MB'] = np.round(self.overall_volume / BITS).astype(int)

    @property
    def number_frames(self):
        """
        Number of L0 slices in the data-take
        """
        return np.ceil(self.simulation['Duration'].values / self.frame_duration).astype(int)

    @property
    def overall_duration(self):
        number_full_frames = self.simulation['Duration'].values // self.frame_duration
        last_frame_duration = (self.simulation['Duration'].values - number_full_frames * self.frame_duration)
        return number_full_frames * (self.frame_duration + self.frame_overlap) + (last_frame_duration + self.frame_overlap)

    @property
    def overall_volume(self):
        return self.overall_duration * self.data_rate() / self.tiff_compression_ratio

    def data_rate(self, swath=None):
        """Compute the data rate for the class in Mbits / s
        The data rate is given for the two RX channels

        if swath is passed then data rate for this swath is retuned
        if not, the mean bit rate is returned

        Parameters:
        -----------
            swath: str in ['S1', 'S2', 'S3', 'SRM']
        Returns:
        ------
            float:
                data rate in Mbits/s

        """
        volume_per_line = self.mean_number_samples * self.complex_sample * self.bits_sample * self.number_channel / MEGA
        #print (volume_per_line, self.prf)
        #print (self.mean_number_samples , self.complex_sample, self.bits_sample , self.number_channel, MEGA)
        rate = volume_per_line * self.prf

        return rate

    def test(self, duration=21):

        vol = duration * self.data_rate() / BITS
        print (f'{vol}[MB]')
        return vol


    def volume_stats(self, title=None):
        df= self._stats('L1A_VOLUME_MB',
                           groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                     'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                           parameter_scaling=[1024, 1024 , 1024, 1024 , 1024 * 1024, 1024 * 1024],
                           out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                            'Global Coverage'],
                           out_column_unit=['GB', 'GB', 'GB', 'GB', 'TB', 'TB'])

        if title is None:
            title = 'BIOMASS L1A Volume Statistics (including slice overlap)'
        return df.style.set_caption(title)

    def frame_stats(self, title=None):
        df= self._stats('L1_FRAME_NUMBER',
                           groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                     'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                           parameter_scaling=[1, 1, 1, 1, 1, 1],
                           out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                            'Global Coverage'],
                           out_column_unit=['#', '#', '#', '#', '#', '#'])
        if title is None:
            title = 'BIOMASS L1A frame number Statistics'
        return df.style.set_caption(title)

class Level1DgmTechBudget(Level1SlcTechBudget):
    # Production model
    number_channel = NUMBER_RX_CHANNEL * 2
    mean_number_samples = 2400  # worst case SW3
    bits_sample = 4 * BITS  # 1 float = 4 bytes = 4 * bITS
    tiff_compression_ratio = 2
    prf = PRF/2 / 5.8 # number of looks)

    def __init__(self, savoir_export_file):
        super().__init__(savoir_export_file)
        self._init()

    def _init(self):
        """
        Compute tech budget elements for the L0 products
        Explicit formulae here
        """

        self.simulation['L1B_VOLUME_MB'] = np.round(self.overall_volume / BITS).astype(int)

    @property
    def overall_duration2(self):
        number_full_frames = self.simulation['Duration'] // self.frame_duration
        last_frame_duration = (self.simulation['Duration'] - number_full_frames * self.frame_duration)
        return number_full_frames * (self.frame_duration + self.frame_overlap) + (last_frame_duration + self.frame_overlap)

    @property
    def overall_volume(self):
        return self.overall_duration * self.data_rate() / self.tiff_compression_ratio

    def data_rate(self,):
        """Compute the data rate for the class in Mbits / s
        The data rate is given for the two RX channels

        if swath is passed then data rate for this swath is retuned
        if not, the mean bit rate is returned

        Parameters:
        -----------
            swath: str in ['S1', 'S2', 'S3', 'SRM']
        Returns:
        ------
            float:
                data rate in Mbits/s

        """
        volume_per_line = self.mean_number_samples * self.bits_sample * self.number_channel / MEGA
        rate = volume_per_line * self.prf

        return rate

    def test(self, duration=21):

        vol = duration * self.data_rate() / BITS
        print(f'{vol}[MB]')
        return vol


    def volume_stats(self, title=None):
        df = self._stats('L1B_VOLUME_MB',
                         groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                   'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                         parameter_scaling=[1024, 1024, 1024, 1024, 1024 * 1024, 1024 * 1024],
                         out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                          'Global Coverage'],
                         out_column_unit=['GB', 'GB', 'GB', 'GB', 'TB', 'TB'])

        if title is None:
            title = 'BIOMASS L1B Volume Statistics (including slice overlap)'
        return df.style.set_caption(title)



class Level1StaTechBudget(Level1SlcTechBudget):
    def __init__(self, savoir_export_file):
        super().__init__(savoir_export_file)
        self._init()

    def _init(self):
        """
        Compute tech budget elements for the L0 products
        Explicit formulae here
        """

        self.simulation['L1C_VOLUME_MB'] = np.round(self.overall_volume / BITS).astype(int)


    def volume_stats(self, title=None):
        df= self._stats('L1C_VOLUME_MB',
                           groupers=['Start', 'Orbit', pd.Grouper(key='Start', freq='D'), 'REPEAT_CYCLE',
                                     'MAJOR_CYCLE', 'GLOBAL_COVERAGE'],
                           parameter_scaling=[1024, 1024 , 1024, 1024 , 1024 * 1024, 1024 * 1024],
                           out_column_name=['Data-Take', 'Orbit', 'Day', 'Repeat Cycle', 'Major Cycle',
                                            'Global Coverage'],
                           out_column_unit=['GB', 'GB', 'GB', 'GB', 'TB', 'TB'])

        if title is None:
            title = 'BIOMASS L1A Volume Statistics (including slice overlap)'
        return df.style.set_caption(title)

