from dataclasses import asdict
import concurrent.futures
import queue
import threading

import datetime
import pathlib
import logging

import pandas as pd



from ...generic_sar.isp.isp_stream import GenericIspStream
from ...generic_sar.isp.isp import GenericIsp

from .sar_isp import BiomassSarIsp, DATA_FIELD_HEADER_LENGTH, PEC_LENGTH, SAR_HEADER_LENGTH
from .sar_isp import BiomassSatIsp
from .ins_isp import BiomassInsIsp
from .isp_structure import DataFieldHeader

from .ispLogger import get_logger

logger = get_logger(__name__)


class ServiceTypeException(RuntimeError):
    pass


class BiomassIspStream(GenericIspStream):
    def __init__(self, ispStreamFile, cal_only=False, limit=None, decode_udf=False):
        super().__init__(ispStreamFile)

        #data frame that contains the stream of ISP decoded
        self.df = None
        self.pipeline = queue.Queue(maxsize=120)
        self._limit = limit
        self._num_isp_read = 0
        self._decode_udf = decode_udf
        self._read_error = 0
        self.isp = None


        if limit is None:
            self._limit = 20 * 60 * 4000 # 20 min * prf=4000Hz


    def pop(self,) -> GenericIsp:
        """
        Method to extract an ISP from the opened file cursor position.

        The ISP is first extract as a generic ISP and the according to the service_type
        it is further specialised into a BiomassSarIsp or BiomassSatIsp or etc


        Parameter
        ---------

        Return
        ------
        GenericIsp, BiomassSarIsp, BiomassSatIsp
            ISP extracted
        """
        # read the generic ISP
        isp = super().pop()

        #check the service type
        if isp.pid >= 25:
            cls = BiomassSarIsp
            self.isp = cls.pop(isp)
        elif isp.pid == 22:
            cls = BiomassSatIsp
            self.isp = cls.pop(isp)
        elif isp.pid in [23,24]:
            cls=BiomassInsIsp
        else:
            raise ServiceTypeException (f'Unknown service type {data_field_header.service_type} at cnt={sequence_count}')

        # return the proper ISP type according to the service type

        return self.isp


    def read(self):
        """Sequentially decode packet headers and store them into a pandas data-frame."""
        packet_counter = 0
        log = logging.getLogger(__name__)
        log.info(f'start decoding: "{self.source}"')

        records = []
        t0 = datetime.datetime.now()
        while (self.fh.tell() < self._file_size) and packet_counter < self._limit:
            try:
                # store last isp
                self.isp = self.pop()
            except:
                log.error(f"Error while reading packet {packet_counter}")
            # Update counter and progress bar
            packet_counter += 1
            records.append(self.isp.decode_header())

        dt = datetime.datetime.now() - t0
        log.info(f"Decoding {packet_counter} ISPs in {dt.total_seconds()} [s]")
        log.info(f"Decoding speed {packet_counter/dt.total_seconds()} [isp/s]")

        self.df = pd.DataFrame(records)

    def to_excel(self, excel_file=None):
        """
        Dump the dataframe into csv file

        Parameter:
        ---------
        excel_file : str

        """
        if self.df is not None:
            if excel_file is None:
                excel_file = pathlib.Path(self.source).with_suffix('.isp.csv')
            else: excel_file = pathlib.Path(excel_file)
            logger.info(f"Writing to {excel_file}")
            self.df.to_csv(excel_file, index=False)
