"""Biomass Instrument Source Packets (ISP) decoder"""
"""according to SAR DATA ICD V4"""
from dataclasses import dataclass, field, InitVar, asdict
from typing import Any, NamedTuple
import enum
import logging
import numpy as np

import bpack
import bpack.bs
from bpack import T
import bitstruct

BITS = bpack.EBaseUnits.BITS
BE = bpack.EByteOrder.BE

## Constants
# Synchronization marker fixed value
SYNC_MARKER = 0x352EF853

class PIDcode(enum.IntEnum):
    """Enum of PID Code
    All the PIDS are summarised in [S2G_ICD] Satellite-to-Ground Segment Interface Control Document
    BIO-ICD-ASU-SY-00003, issue 7.0
    """
    obc_time = 0  # OBC time packet
    plat_anc = 22 # Platform ancillary
    ins_anc_h = 23 #Instrument Ancillary Packet (low rate) - H pol
    ins_anc_v = 24 #Instrument Ancillary Packet (low rate) - V pol
    sm_rx_h = 25  # Instrument science data (Measurement mode) –H polarization
    sm_rx_v = 26  # Instrument science data (Measurement mode) –V polarization
    ro_rx_h = 27  # Instrument science data (Receive only mode) – H polarization
    ro_rx_v = 28  # Instrument science data (Receive only mode) – V polarization
    sim_rx_h = 29  # Instrument science data (Simulation mode) – H polarization
    sim_rx_v = 30  # Instrument science data (Simulation mode) – V polarization
    spare1_rx_h = 31  # Instrument science data (spare 1 - H)
    spare1_rx_v = 32  # Instrument science data (spare 1 - V)
    spare2_rx_h = 33  # Instrument science data (spare 2 - H)
    spare2_rx_v = 34  # Instrument science data (spare 2 - V)
    extcal_rx_h = 35  # Instrument science data (External cal) - H polarization
    extcal_rx_v = 36  # Instrument science data (External cal) - V polarization


class BAQBLcode(enum.IntEnum):
    complex_64 = 0  # 64 complex samples
    complex_128 = 1  # 128 complex samples


class GSTLcode(enum.IntEnum):
    s1_int = 0  # Round orbit for swath 1 on INT measurement
    s2_int = 1  # Round orbit for swath 2 on INT measurement
    s3_int = 2  # Round orbit for swath 3 on INT measurement
    s1_tom = 3  # Round orbit for swath 1 on TOM measurement
    s2_tom = 4  # Round orbit for swath 2 on TOM measurement
    s3_tom = 5  # Round orbit for swath 3 on TOM measurement
    ro = 6  # Round orbit for receive only mode
    ext_cal = 16  # External calibration sequence
    simulation = 47  # Short timeline for simulation mode
    # all the not booked codes 7-15 and 17-46 are free for future uses


class GpreCode(enum.IntEnum):
    odd_rank_preamble = 0  # Preamble sequence for odd rank used in measurement mode
    even_rank_preamble = 1  # Preamble sequence for even rank used in measurement mode
    # 2 and 3 are free for future use
    dummy2 = 2
    dummy3 = 3


class GposCode(enum.IntEnum):
    odd_rank_postamble = 0  # Preamble sequence for odd rank used in measurement mode
    even_rank_postamble = 1  # Preamble sequence for even rank used in measurement mode
    # 2 and 3 are free for future use
    dummy2 = 2
    dummy3 = 3


class GcalCode(enum.IntEnum):
    odd_rank_calibration = 0  # Interleaved calibration sequence for odd rank used in measurement mode
    even_rank_calibration = 1  # Interleaved calibration sequence for even rank used in measurement mode
    # 2 and 3 are free for future use
    dummy2 = 2
    dummy3 = 3



class BAQMODcode(enum.IntEnum):
    baq_4 = 0  # BAQ 4 bit
    baq_5 = 1  # BAQ 5 bit
    baq_6 = 2  # BAQ 6 bit
    bypass = 3  # BAQ bypassed
    # all other values are invalid


class STYPcode(enum.IntEnum):
    txv_rx = 0  # Tx in V - Pol, Receive in V & H
    txh_rx = 1  # Tx in H - Pol, Receive in V & H
    rx_only = 2  # Receive Only in V & H
    txv_only = 3  # Transmit Only in V - Pol
    txh_only = 4  # Transmit Only in H - Pol
    txCal_v = 5  # Tx Calibration of PAS - V
    txCal_h = 6  # Tx Calibration of PAS - H
    txCal_v_1 = 7  # Tx Calibration of SSPA1 in PAS - V
    txCal_v_2 = 8  # Tx Calibration of SSPA2 in PAS - V
    txCal_h_1 = 9  # Tx Calibration of SSPA1 in PAS - H
    txCal_h_2 = 10  # Tx Calibration of SSPA2 in PAS - H
    rxCal = 11  # Rx Calibration
    shCal = 12  # Short Loop Calibration in CDN
    idle = 13  # No Transmit and no Receive
    txv_rxs = 14  # Same settings as TxV&Rx but needed in ICAL to identify stabilisation prior TxCal
    txh_rxs = 15  # Same settings as TxH&Rx but needed in ICAL to identify stabilisation prior TxCal
    noise = 16  # Same settings as RxOnly but useful to clearly identify noise measurement
    # 17 - 31 not used



class AOCSModeCode(enum.IntEnum):
    idle = 0
    ash = 1
    normal = 2
    ocm = 3
    rdm = 4


class AOCSNMSubModeCode(enum.IntEnum):
    stopped = 0
    sup = 1
    gap = 2
    man = 3
    cap = 4


class OBTSyncStatusCode(enum.IntEnum):
    free_running = 0
    synchronising = 1
    synchronised = 2

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class DataFieldHeader:
    """
    Data Field Header of the BIOMASS SAR ISP

    """
    filler_1: T['u1'] = bpack.field(default=0, repr=False)
    version_number: T['u3'] = 1  # FIXED 1 corresponds to the ECSS-E-70-41A
    filler_2: T['u4'] = bpack.field(default=0, repr=False)
    service_type: T['u8'] = 211  # FIXED 211 means SAR data management service
    service_subtype: T['u8'] = 1  # FIXED 1 means SAR data TM
    destination_id: T['u8'] = 0  # FIXED 0 means Ground
    coarse_time: T['u32'] = 0
    fine_time: T['u16'] = 0

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

    def asdict(self):
        return bpack.asdict(self)


@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class OBTSyncStatus:
    npe: T['u1'] = 0  # No PPS Error. 0 means no error
    nle: T['u1'] = 0  # No LOBT Error. 0 means no error
    mss: T['u1'] = 0  # Master Sync Status. 0 means not synchronized
    tty: T['u1'] = 0  # Time Type. 0 means no update since reset
    tmr: T['u1'] = 0  # Time Message Received. 0 means TM Message not received
    smt: T['u1'] = 0  # Sync Method. 0/1 means synchronized with SpW/PPS
    sst: T['u1'] = 0  # Sync Status. 0 means not synchronized
    sen: T['u1'] = 0  # Sync Enabled. 0 means disabled

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)
    def asdict(self):
        return bpack.asdict(self)

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class DatationEnhancementService:
    fine_time_byte2: T['u8'] = 0  # 3rd byte of fine time
    obt_sync_status: OBTSyncStatus = OBTSyncStatus()

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

    def asdict(self):
        d = {'fine_time_byte2' : self.fine_time_byte2}
        d.update(self.obt_sync_status.asdict())
        return d


@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class IndexService:
    sar_packet_count: T['u32'] = 0
    pri_count: T['u32'] = 0

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)
    def asdict(self):
        return bpack.asdict(self)


@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class DataTakeInformationService:
    baq_block_length: BAQBLcode = bpack.field(size=1, default=BAQBLcode.complex_128)
    decimation_bypassed: bool = False  # 0/False means Decimation filtering performed
    instrument_configuration_id: T['u30'] = 0
    data_take_id: T['u32'] = 0  # TODO consolidate definition
    ground_user_annotation: T['u32'] = 0  # TODO consolidate definition
    gstl_index: GSTLcode = bpack.field(size=6, default=GSTLcode.s1_int)
    warmup_flag: bool = False  # 0/False means no warmup sequence used
    preamble_enabled: bool = False  # 0/False means preamble sequence used TODO check with Airbus since semantically it should be in the opposite way
    gpre_index: GpreCode = bpack.field(size=2, default=GpreCode.odd_rank_preamble)
    postamble_enabled: bool = False  # 0/False means postamble sequence used TODO check with Airbus since semantically it should be in the opposite way
    gpos_index: GposCode = bpack.field(size=2, default=GposCode.odd_rank_postamble)
    calibration_enabled: bool = False  # 0/False means interleaved calibration sequences used TODO check with Airbus since semantically it should be in the opposite way
    gcal_index: GcalCode = bpack.field(size=2, default=GcalCode.odd_rank_calibration)

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

    def asdict(self):
        return bpack.asdict(self)

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class BITSELcode:
    bitsel_length: T['u4'] = 0
    bitsel_start: T['u4'] = 0



@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class SSPAControlMessage:
    iv_sign: T['u1'] = 0
    iv_code: T['u11'] = 0  # In-phase Voltage IV=iv_sign*5*iv_code/4096
    qv_sign: T['u1'] = 0
    qv_code: T['u11'] = 0  # Quadrature Voltage QV=qv_sign*5*qv_code/4096
    epcv_code: T['u6'] = 0  # auxiliary drain voltage EPCV=12*epcv_code/64
    tx_enable: bool = 0  # This flag is combined with the TxGate signal. It is set for Tx on.
    parity: T['u1'] = 0

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class SSPAMessageIndex:
    sidx_fill: T['u3'] = 0  # unused bits
    sidx_code: T['u5'] = 0 # Each SSPA message index has a value range 0 – 31 and is located in the 5 lsbs of each byte.

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class RealTimeParameterService:
    pulse_repetition_interval: T['u16'] = 0
    sampling_window_position: T['u16'] = 0
    sampling_window_length: T['u16'] = 0
    bit_selection: BITSELcode = BITSELcode()
    baq_mode: BAQMODcode = bpack.field(size=3, default=BAQMODcode.baq_4)
    signal_type: STYPcode = bpack.field(size=5, default=STYPcode.txv_rx)
    tx_pulse_index: T['u6'] = 0  # TODO TXPIDXcode class definition after TBD resolution in SAR data ICD
    rank: T['u5'] = 0
    temperature_compensation: bool = False  # 0/False means PAS temperature compensation disabled
    spares_1: T['u4'] = bpack.field(default=0, repr=False)
    tx_pulse_length: T['u16'] = 0
    sspa_contol_message_v_n1: SSPAControlMessage = SSPAControlMessage()
    sspa_contol_message_v_n2: SSPAControlMessage = SSPAControlMessage()
    sspa_contol_message_v_r: SSPAControlMessage = SSPAControlMessage()
    sspa_contol_message_h_n1: SSPAControlMessage = SSPAControlMessage()
    sspa_contol_message_h_n2: SSPAControlMessage = SSPAControlMessage()
    sspa_contol_message_h_r: SSPAControlMessage = SSPAControlMessage()
    ras_control_message_v: T['u16'] = 0 # TODO RASControlMessage class definition after TBD resolution in SAR data ICD
    ras_control_message_h: T['u16'] = 0 # TODO RASControlMessage class definition after TBD resolution in SAR data ICD
    sspa_message_index_0: SSPAMessageIndex = SSPAMessageIndex()
    sspa_message_index_1: SSPAMessageIndex = SSPAMessageIndex()
    sspa_message_index_2: SSPAMessageIndex = SSPAMessageIndex()
    sspa_message_index_3: SSPAMessageIndex = SSPAMessageIndex()
    sspa_message_index_4: SSPAMessageIndex = SSPAMessageIndex()
    sspa_message_index_5: SSPAMessageIndex = SSPAMessageIndex()
    spares_2: T['u16'] = bpack.field(default=0, repr=False)

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

    @property
    def pri(self):
        return self.pulse_repetition_interval

    @property
    def swp(self):
        return self.sampling_window_position

    @property
    def swl(self):
        return self.sampling_window_length

    def asdict(self):
        d = {'pri' : self.pulse_repetition_interval,
             'swp': self.sampling_window_position,
             'swl': self.sampling_window_length,
             'rank': self.rank,
             'bitsel_length' : self.bit_selection.bitsel_length,
             'bitsel_start' : self.bit_selection.bitsel_start,
             'baq_mode': self.baq_mode,
             'sigtyp' : self.signal_type,
             'txpl_index' :  self.tx_pulse_index,
             'txpl' : self.tx_pulse_length
             }
        return d

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class SarHeader:
    datation_enhancement_service: DatationEnhancementService = DatationEnhancementService()
    synchronization_service: T['u32'] = SYNC_MARKER  # FIXED to 0x352EF853
    index_service: IndexService = IndexService()
    data_take_information_service: DataTakeInformationService = DataTakeInformationService()
    real_time_parameter_service: RealTimeParameterService = RealTimeParameterService()

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

    def asdict(self):

        #r = bpack.asdict(self)
        d = self.datation_enhancement_service.asdict()
        d.update({'sync_marker' : self.synchronization_service})
        d.update(self.index_service.asdict())
        d.update(self.data_take_information_service.asdict())
        d.update(self.real_time_parameter_service.asdict())
        return d


@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class PacketErrorCount:
    pec: T['u16'] = 0

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class SarSourceData:
    ''' dataclass for the source data field
                     source data field
        |-------------------------------Source-Packet-------------------------------------|
        |--Packet-Header--|--------Packet-Data-Field--------------------------------------|
               (6)        |--data field header---|---------------source-data--------|-pec-|
        >>>>>>>>                   (10)          |--SAR-header--|---user--data------|
                                                        (76)
    '''
    sar_header: SarHeader = SarHeader()

    def __post_init__(self):
        self.user_data = field(default=None, repr=False)



@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class SatSourceData:
    ''' dataclass for the source data field
    '''
    aocs_version: T['u16'] = 0
    orbit_drift_phase_flag: bool = bpack.field(size=32, default=False)
    nav_ut_day_mjd: T['f64'] = 0  # [days]
    nav_ut_fraction_day_mjd: T['f64'] = 0  # [fraction of day]
    nav_delta_time: T['f64'] = 0  # [seconds]
    nav_successive_valid_measures: T['u32'] = 0  # Number of successive valid GPS measurements
    nav_gnss_pv_date_obt: T['f64'] = 0  # [seconds]
    nav_gnss_pvt_validity: bool = bpack.field(size=32, default=False)  # Current GNSS PVT validity (for a number of successive measurements)
    nav_prop_pos_cog_ecef_x: T['f64'] = 0  # [meters]
    nav_prop_pos_cog_ecef_y: T['f64'] = 0  # [meters]
    nav_prop_pos_cog_ecef_z: T['f64'] = 0  # [meters]
    nav_prop_vel_cog_ecef_x: T['f64'] = 0  # [meters/seconds]
    nav_prop_vel_cog_ecef_y: T['f64'] = 0  # [meters/seconds]
    nav_prop_vel_cog_ecef_z: T['f64'] = 0  # [meters/seconds]
    iae_dse_estim_quat_s: T['f64'] = 0  # rotation angle
    iae_dse_estim_quat_x: T['f64'] = 0  # rotation axis X
    iae_dse_estim_quat_y: T['f64'] = 0  # rotation axis Y
    iae_dse_estim_quat_z: T['f64'] = 0  # rotation axis Z
    iae_dse_estim_ang_rate_x: T['f64'] = 0  # angular rate [rad/s]
    iae_dse_estim_ang_rate_y: T['f64'] = 0  # angular rate [rad/s]
    iae_dse_estim_ang_rate_z: T['f64'] = 0  # angular rate [rad/s]
    aocs_mode: AOCSModeCode = bpack.field(size=32, default=AOCSModeCode.idle)
    aocs_nm_state: AOCSNMSubModeCode = bpack.field(size=32, default=AOCSNMSubModeCode.stopped)
    star_tracker_quality_status: T['u32'] = 0  # (bit0:OH1, bit1:OH2, bit3:OH3, bit4:OH4)
    obt_sync_status: OBTSyncStatusCode = bpack.field(size=32, default=OBTSyncStatusCode.free_running)
    instrument_heater_ctrl_thermistors: T['u144'] = 0
    instrument_monitoring_thermistors: T['u256'] = 0
    zd_commanded_quat_s: T['f64'] = 0  # rotation angle
    zd_commanded_quat_x: T['f64'] = 0  # rotation axis X
    zd_commanded_quat_y: T['f64'] = 0  # rotation axis Y
    zd_commanded_quat_z: T['f64'] = 0  # rotation axis Z
    zd_commanded_ang_rate_x: T['f64'] = 0  # angular rate [rad/s]
    zd_commanded_ang_rate_y: T['f64'] = 0  # angular rate [rad/s]
    zd_commanded_ang_rate_z: T['f64'] = 0  # angular rate [rad/s]
    ops_angle_ecef: T['f32'] = 0  # Angle [rad] between the satellite position and the ascending node in the orbital plane
    roll_steering_profile_index: T['u32'] = 0  # Roll steering harmonic profile index, 1 to 6.
    orbit_number: T['u32'] = 0  # Absolute orbit number set to “0” at start of the NAV function.

    def asdict(self):
        return  bpack.asdict(self)

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class SarPacketDataField:
    ''' dataclass for the packet data field
        |-----------------Source-Packet---------------------------------------|
        |--Packet-Header--|--------Packet-Data-Field--------------------------|
       >>>>>>             |--data field header---|-------source-data----|-pec-|
    '''
    data_field_header: DataFieldHeader = DataFieldHeader()
    source_data: SarSourceData = SarSourceData()
    def __post_init__(self):
        self.pec = field(default=None, repr=False)


@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class SatPacketDataField:
    '''    ############################  BIOMASS packet layout ###################################
            #    |-------------------------------Source-Packet-------------------------------------|
            #    |--Packet-Header--|--------Packet-Data-Field--------------------------------------|
            #           (6)        |--data field header---|---------------source-data--------|-pec-|
            #                               (10)                         (284)                 (2)
        #########################################################################################
    '''
    data_field_header: DataFieldHeader = DataFieldHeader()
    source_data: SatSourceData = SatSourceData()
    def __post_init__(self):
        self.pec = field(default=None, repr=False)

    def asdict(self):
        return  bpack.asdict(self)
