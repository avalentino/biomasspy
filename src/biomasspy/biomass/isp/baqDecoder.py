import logging
import concurrent.futures
import queue
import threading
import datetime

import pathlib

import numpy as np
import pandas as pd

from scipy.special import erf
from scipy.special import erfinv

from  .ispLogger import get_logger
logger = get_logger(__name__)
logger.setLevel(logging.INFO)



class BaqThresholdTable:
    """
        Get the threshold from the RDB/BCT from an excel file.
        This will have to be done from the RDB in the future

        The right table is retrieved using the index operator with the right baqmode_code

        Example: for baqmode 5
        baq_table=BaqThresholdTable()
        Threshold = baq_table[5]
    """
    def __init__(self, bct_table_file, rdb_version=2):

        self.table_file = pathlib.Path(bct_table_file)
        if self.table_file.exists():
            self._df = pd.read_excel(str( self.table_file ))

        self.rdb_version = 2

        self._load_table(self.rdb_version)


    def _load_table(self, rdb_version):
        baqmode_list = self._df['BAQMOD'].unique()
        self.thresh_table = {}
        for baqmod_ in baqmode_list:
            ix = (self._df['RDB'] == rdb_version)  & (self._df['BAQMOD'] == baqmod_ )
            mcode = self._df.loc[ix, 'mcode'].values
            thresh = self._df.loc[ix, 'Norm. Sigma Threshold'].values
            self.thresh_table[baqmod_] = dict(zip(mcode, thresh))


    def __getitem__(self, baqmode):
        "Returns the dict mcode <-> Threshold for the relevant BAQMODE"
        return self.thresh_table.get(baqmode, None)



class BaqDecoder():
    """Class performing the decoding of one ISP. Break ths ISP into BAQ blocks
        and spread them over different threads.
    """

    def __init__(self, user_data_field, number_sample, baq_threshold_table, \
                l_asm=16, baqmode=4, baqbl=128,\
                num_decoding_thread=None, \
                verbose=False):
        """
            initialie the BAQ decoder for BIOMASS mission
            Parameters:
            ----------
            user_data_field : numpy array of bytes uint8
                It shall represent the exact user data field without header and without the
                packet error control

        """
        if verbose:
            logger.setLevel(logging.DEBUG)
        #make some assertions taken from the SAR Data ICD
        assert(isinstance(user_data_field, np.ndarray)), 'user_data_field shall be a numpy.ndarray'
        assert(np.mod(user_data_field.nbytes*8, 16.) == 0), 'The user_data_field is not aligned with a 16 bits word boundary'
        assert(baqmode in [4, 5, 6]), 'baqmode shall be in [4,5,6]'
        assert(baqbl in [64, 128]), 'baqbl shall be in [64, 128]'

        self.user_data_field = user_data_field
        self.l_asm = l_asm
        self.baqmode = baqmode
        self.baqbl = baqbl
        self.number_sample = number_sample
        self.baq_threshold_table = baq_threshold_table
        self._num_workers = num_decoding_thread

        self.udf_decoded = np.zeros(self.number_sample, dtype=np.complex)


    #The indexing operators allow to get/set the ith BAQ block
    #where i is [0, self.number_block]
    def __getitem__(self, i):
        s0 = i * self.baqbl
        s1 = s0+self.baqbl
        return self.udf_decoded[s0:s1]

    def __setitem__(self, i, value):
        s0 = i * self.baqbl
        s1 = np.clip(s0+self.baqbl, 0, self.number_sample)
        ll = s1-s0
        self.udf_decoded[s0:s1] = value[:ll]

    @property
    def number_block(self):
        return np.ceil(self.number_sample/ self.baqbl)


    @property
    def block_size_bits(self):
        """number of bits composing a BAQ block"""
        return self.l_asm + self.baqbl * 2 * self.baqmode

    @property
    def l_sar_bits(self):
        """Equation 5-1 of SAR Data ICD
            Return the total lenght withoout filler bits in bits
        """

        return self.number_block*self.l_asm + self.number_sample * 2 * self.baqmode

    @property
    def filler_bits(self):
        return self.user_data_field.nbytes * 8 - self.l_sar_bits


    def start(self):
        #the queue will be filled with all the blocks

        self._pipeline = queue.Queue(maxsize=self.number_block)
        if self._num_workers is None:
            self._num_workers = int(self.number_block/2)

        logger.debug(f"Start Decoding ...with {self._num_workers+1} workers")
        self.lock = threading.Lock()
        self.event = threading.Event()

        t0 = datetime.datetime.now()
        with concurrent.futures.ThreadPoolExecutor(max_workers=self._num_workers+1) as executor:
            executor.submit(self.__baq_producer)
            for i in range(self._num_workers):
                name = f"{i+1}"
                executor.submit(self.__baq_consumer, name=name)
        t1 = datetime.datetime.now()

        dt = t1 - t0
        logger.debug(f"Start Decoding ...Done")




    def __baq_producer(self):
        """ Read the block sequentially from the udf and put them in the queue
            for decoding by the consumers
        """
        logger.debug("In __baq_producer ")
        try:
# TODO:  : safer to use split rather than array split
            self.block_list = np.split(self.user_data_field, self.number_block)
        except:
            logger.error (f"Error while chunking... {self.user_data_field.size} / {self.number_block}")

        for bix, block_ in enumerate(self.block_list):
            self._pipeline.put((bix, block_))
            logger.debug (f"Filling pipeline {bix} q={self._pipeline.qsize()}")

            #condition to exit
            if bix == len(self.block_list)-1:
                self.event.set()



    def __baq_consumer(self, name=None):
        """
            Perform actual decoding
        """

        logger.debug(f"In __baq_decoder {name}")
        ii = 0
        while not self.event.is_set() or not self._pipeline.empty():
            block_ix, baq_block = self._pipeline.get()

            logger.debug(f"[{name}] trigger decoding of block {block_ix}")

            block_decoded = self.baq_decode(baq_block)
            #recompose the final ISP
            self.lock.acquire()
            self.__setitem__(block_ix, block_decoded)
            self.lock.release()
        logger.debug(f"Consumer {name} received event. Exiting")



    def baq_decode(self, block):
        try:
            baq_block = BiomassBaqBlock(block, self.baqmode, self.baqbl, self.baq_threshold_table)
        except:
            logger.debug('Error while decoding single block')

        ret = baq_block.mi_code + 1j* baq_block.mq_code

        return ret



    @staticmethod
    def _number_sample(swl, decimation_factor):
        return floor(SWL/decimation_factor)

    @staticmethod
    def _number_baq_blocks(number_sample, baqbl):
        return ceil(number_sample/ baqbl)

    @staticmethod
    def _baq_block_size_bits(l_asm, baqbl, baqmod):
        return l_asm + baqbl * 2 * baqmod

    @staticmethod
    def _packet_size_bits(l_asm, nblock, Ns, baqmod):
        return nblock*l_asm + Ns*2*baqmod

    @staticmethod
    def simulate_udf( ns_complex, baqmode, baqbl):
        if (baqmode-1 < 8):
            tt=np.uint8
        else: tt=np.uint

        num_block = np.ceil(ns_complex/ baqbl)

        maxval = 2**(baqmode-1)

        #we nneed complex samples hence two
        code = np.mod(np.arange(num_block*baqbl*2,dtype=tt), maxval)


        block_list=np.split(code, num_block)

        for bix, block_ in  enumerate(block_list):

            #add asm

            mcode_bits = np.unpackbits(block_)
            sign = np.mod(block_,2)

            mcode_bits = mcode_bits.reshape(-1,8)

            mcode_bits[:,8-baqmode]=sign
            mcode_bits = mcode_bits[:,8-baqmode:]
            mcode_bits = mcode_bits.reshape(baqmode*baqbl*2)
            mcode_= np.packbits(mcode_bits)
            mcode_= np.pad(mcode_,( 2,0), 'constant', constant_values=(100,100))
            if bix==0:
                mcode = mcode_
            else:
                mcode=np.append(mcode,mcode_)
        return code, mcode





class BiomassBaqBlock:
    """Class to perform the BAQ decoding a single block
        1: reading ASM
        2: getting the sign

    """
    #lenght of the ASM in bits
    l_asm = 16
    decimation_factor = 10

    #taken from the RDB MPT.
    asm_small_limit = dict(zip([4, 5, 6], [1, 3, 7]))

    def __init__(self, block, baqmode, baqbl, baq_threshold=None):

        assert(baqmode in [4, 5, 6]), 'baqmode shall be in [4,5,6]'
        assert(baqbl in [64, 128]), 'baqbl shall be in [64, 128]'

        #number of real samples in the BAQ bloq
        # the division in I/Q will be done later
        self.number_samples = 2 * baqbl
        self.baqmode = baqmode
        self.baq_threshold = baq_threshold

        self.block = block



        self.__unpack_mcode()
        try:
            if self.baq_threshold is not None:
                self.decoded_block = self.__sample_reconstruction()
        except:
            raise NotImplementedError


    def get_mcode(self, iq=False):
        if iq is True:
            return self.m_code[::2], self.m_code[1::2]
        else:
            return self.m_code
        #taking odd and even to recompose the I/Q part


    def get_mcode_sign(self, iq=False):
        if iq is True:
            return self.m_code_sign[::2], self.m_code_sign[1::2]
        else:
            return self.m_code_sign


    def __sample_reconstruction(self):
        asm_value = self.compute_asm()

        mcode = self.get_mcode()
        msign = self.get_mcode_sign()
        #normal reconnstruction
        if asm_value > self.asm_small_limit[self.baqmode]:
            norm = self.__norm_value(mcode)
            sample = msign * asm_value * norm

        else:
            #simple reconstruction scheme
            raise NotImplementedError
            sample = mcode


        return sample[::2] + 1j* sample[1::2]




    def threshold_value(self, mcode):
        """threshold values for the erf function"""
        l_baq =  self.baqmode
        if mcode == -1:
            t = 0
        elif mcode < 2**(l_baq-1)-1:
            t = self.baq_threshold[mcode]

        elif mcode == 2**(l_baq-1)-1:
            t = 1

        return t


    def __norm_value(self, mcode):
        """This function transform the Mcode into nornmalised values"""
        spi = np.sqrt(np.pi)

        T = self.threshold_value
        t0 = erf( T(mcode)   / spi )
        t1 = erf( T(mcode-1) / spi )
        norm = spi * erfinv((t0+t1)/2)

        return norm


    def compute_asm(self):
        # TODO: computing the Average Signal Magnitude
        # It might be safer to unpack the bits rather than using a view
        # this should checked later
        l_asm_byte = int(self.l_asm / 8)
        return self.block[0:l_asm_byte].view(dtype=np.int16)


    def __unpack_mcode(self):
        """THis functions aims at unpacking the bitstring and retrieving
        an array of Mcodes and sign associated
        """
        l_asm_byte = int(self.l_asm / 8)
        bl_bits = np.unpackbits(self.block[l_asm_byte:])
        self.bl_bits = bl_bits

        assert(bl_bits.size % self.baqmode == 0), "The number of bits shall be a multiple of BAQMOD"

        #reshapping the bit sequence in real samples
        self.sample_list = bl_bits.reshape(self.number_samples, self.baqmode )

        #reading, interpreting and nulling the sign bit
        sign = self.sample_list[:,0]
        self.m_code_sign = (-1)**sign
        self.sample_list[:, 0] = 0

        #padding the sample to 8 bits  to perform a proper packing
        _value = np.pad(self.sample_list, ((0,0), (8-self.baqmode,0)), \
                        'constant', constant_values=(0,0))
        self.m_code = np.packbits(_value)
