from dataclasses import dataclass, field, InitVar, asdict

from typing import Any, NamedTuple
import numpy as np

from .sar_isp import DATA_FIELD_HEADER_LENGTH
from ...generic_sar.isp.isp import PacketHeader, GenericIsp
from .isp_structure import DataFieldHeader, PacketErrorCount


@dataclass
class InformationBlock:
    asw_version : int = 0
    rdb_version: int = 0
    icid: int = 0
    pas_ras_status : int = 0

@dataclass
class DcuTemperature:
    tm : np.ndarray = np.zeros(5, dtype=np.int)

@dataclass
class PasTemperature:
    tm: np.ndarray = np.zeros(32, dtype=np.int)

@dataclass
class RasTemperature:
    tm: np.ndarray = np.zeros(6, dtype=np.int)

@dataclass
class TxtgStatus:
    tm: np.ndarray = np.zeros(9, dtype=np.int)

@dataclass
class RxmStatus:
    tm: np.ndarray = np.zeros(16, dtype=np.int)

@dataclass
class SourceData:
    info_block : InformationBlock = InformationBlock()
    dcu_temp : DcuTemperature = DcuTemperature()
    pas_temp : PasTemperature = PasTemperature()
    ras_temp : RasTemperature = RasTemperature()
    txtg_status :TxtgStatus = TxtgStatus()
    rxm_status : RxmStatus = RxmStatus()


@dataclass
class PacketDataField:
    ''' dataclass for the packet data field
        |-----------------Source-Packet---------------------------------------|
        |--Packet-Header--|--------Packet-Data-Field--------------------------|
       >>>>>>             |--data field header---|-------source-data----|-pec-|
    '''
    data_field_header: DataFieldHeader = DataFieldHeader()
    source_data: SourceData = field(default=SourceData(), repr=False)
    packet_error_control: PacketErrorCount = PacketErrorCount()

    @classmethod
    def from_packet_data_field(cls, packet_data_field_buffer):

        data_field_header = DataFieldHeader.frombytes(packet_data_field_buffer[:DATA_FIELD_HEADER_LENGTH])

        return cls(data_field_header=data_field_header, source_data=SourceData())

@dataclass
class BiomassInsIsp(GenericIsp):
    """ Top class contain a BIOMASS SAR Source Packet
            |-----------------Source-Packet-----------------------|
            |--Packet-Header--|--------Packet-Data-Field----------|
    """
    packet_header: PacketHeader = PacketHeader()
    packet_data_field: PacketDataField = PacketDataField()
    decode_user_data: InitVar[bool] = False

    def __post_init__(self, decode_user_data):
        """post initialisation for triggering the decoding of the user_data

        Parameter
        --------
        decode_user_data : Bool
            this is passed as the InitVar from the init parent method
        """
        #if decode_user_data is True:
        #    self.user_data = self.decode(self.user_data)
        #else:
        #    self.user_data = np.frombuffer(self.user_data, dtype=np.uint8)
        #raise NotImplementedError
        pass
    @classmethod
    def from_packet_data_field(cls, packet_header: PacketHeader,
                               packet_data_field_buffer: bytes,
                               decode_user_data: bool = False):

        assert(len(packet_data_field_buffer) == packet_header.packet_lenght+1), \
                f'buffer shall be of size {packet_header.packet_lenght+1} bytes'

        #factory mehtod for the decoded packet data field
        packet_data_field = PacketDataField.from_packet_data_field(packet_data_field_buffer)

        return cls(packet_header=packet_header,
                   packet_data_field=packet_data_field,
                   decode_user_data=decode_user_data)
