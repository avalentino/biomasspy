from dataclasses import dataclass, field, InitVar, asdict
import numpy as np

from ...orbit.genericOrbitInterpolator import GenericOrbitInterpolator
from ...generic_sar.isp.isp import GenericIsp, PacketHeader
from .isp_structure import SarPacketDataField, SatPacketDataField , PacketErrorCount, SYNC_MARKER

__all__ = ['BiomassSarIsp', 'BiomassSatIsp']

PEC_LENGTH = 2 #bytes
DATA_FIELD_HEADER_LENGTH = 10 #bytes
SAR_HEADER_LENGTH = 76
F_USO = 37.826087
# Ultra Stable Oscillator Frequency
ASM_L = 13
# Decimation Factor
DEC_FACTOR = 5

class SyncMarkerException(RuntimeError):
    pass

@dataclass
class BiomassSatIsp(GenericIsp):
    ''' Top class contain a BIOMASS Satellite Source Packet as defined BIO.ICD.ASU.SY.00003
    ############################  BIOMASS packet layout ###################################
    #    |-------------------------------Source-Packet-------------------------------------|
    #    |--Packet-Header--|--------Packet-Data-Field--------------------------------------|
    #           (6)        |--data field header---|---------------source-data--------|-pec-|
    #                               (10)                         (284)                 (2)
    #########################################################################################


    '''
    packet_header: PacketHeader = PacketHeader()
    packet_data_field: SatPacketDataField = SatPacketDataField()

    @property
    def pec(self):
        return self.packet_data_field.pec
    @pec.setter

    def pec(self, pec_val):
        assert (len(pec_val) == PEC_LENGTH), f"Wrong PEC Length"
        self.packet_data_field.pec = PacketErrorCount.frombytes(pec_val)

    def decode_header(self):
        r = self.packet_header.asdict()
        r.update(self.packet_data_field.data_field_header.asdict())
        r.update(self.packet_data_field.source_data.asdict())

        return r

    @property
    def datation(self) -> float:
        """Perform the calculation of the ISP GPS time"""
        ct = self.packet_data_field.data_field_header.coarse_time
        ft = self.packet_data_field.data_field_header.fine_time
        # todo: using 1e6 in the code below is to cope for an error with the data. to be removed later
        fuso = F_USO * 1e6
        ft_sec = ft * 4 / fuso
        return GenericOrbitInterpolator.to_datetime64(ct + ft_sec)

    def sat_pos(self):
        dim = ['x', 'y', 'z']
        rr = 'nav_prop_pos_cog_'
        pos = []

        for d in dim:
            pos.append(self.df[rr+d].values)
        return pos

    @staticmethod
    def to_datetime64( mjd2k):
        return GenericOrbitInterpolator.to_datetime64(mjd2k)

    @classmethod
    def pop(cls, isp: GenericIsp):
        packet_header = isp.packet_header
        user_data = isp.packet_data_field[DATA_FIELD_HEADER_LENGTH:-PEC_LENGTH]
        pec = isp.packet_data_field[-PEC_LENGTH:]
        # it is not possible to decode with bpack the user_Data as the size is variable
        # hence only the header are decoded and user data and pec  are treated later
        packet_data_field = SatPacketDataField.frombytes(isp.packet_data_field)
        isp = cls(packet_header=packet_header, packet_data_field=packet_data_field)
        isp.user_data = user_data
        isp.pec = pec

        return isp


@dataclass
class BiomassSarIsp(GenericIsp):
    """ Top class containing a BIOMASS SAR Source Packet

        |-------------------------------Source-Packet-------------------------------------|
        |--Packet-Header--|--------Packet-Data-Field--------------------------------------|
               (6)        |--data field header---|---------------source-data--------|-pec-|
                                   (10)          |--SAR-header--|---user--data------|
                                                        (76)
    """
    packet_header: PacketHeader = PacketHeader()
    packet_data_field: SarPacketDataField = SarPacketDataField()

    @classmethod
    def pop(cls, isp: GenericIsp):
        """
        This method extracts from a generic isp a Biomass SAR ISP

        The packet header is copied the packet data field is instead decoded

        The actual user data is not decoded (BAQ decoding) but is left available as bytes for
        later decoding following the proper unpacking (BAQ, BYPASS,...)

        """
        packet_header = isp.packet_header
        user_data = isp.packet_data_field[DATA_FIELD_HEADER_LENGTH + SAR_HEADER_LENGTH:-PEC_LENGTH]
        pec = isp.packet_data_field[-PEC_LENGTH:]

        # it is not possible to decode with bpack the user_Data as the size is variable
        # hence only the header are decoded and user data and pec  are treated later
        packet_data_field = SarPacketDataField.frombytes(isp.packet_data_field)
        isp = cls(packet_header=packet_header, packet_data_field=packet_data_field)

        isp.user_data = user_data
        isp.pec = pec

        if isp.test_sync is False:
            raise SyncMarkerException(f'pri_counter: {isp.pri_counter + 1}')
        return isp


    @property
    def user_data(self):
        return self.packet_data_field.source_data.user_data

    @user_data.setter
    def user_data(self, ud):
        assert (len(ud) == self.packet_header.packet_data_field_length + 1 - DATA_FIELD_HEADER_LENGTH -
                SAR_HEADER_LENGTH - PEC_LENGTH), f"Wrong User Data Length"
        self.packet_data_field.source_data.user_data = ud

    @property
    def pec(self):
        return self.packet_data_field.pec

    @pec.setter
    def pec(self, pec_val):
        assert (len(pec_val) == PEC_LENGTH), f"Wrong PEC Length"
        self.packet_data_field.pec = PacketErrorCount.frombytes(pec_val)

    def decode_header(self):
        r = self.packet_header.asdict()
        r.update(self.packet_data_field.data_field_header.asdict())
        r.update(self.packet_data_field.source_data.sar_header.asdict())
        return r

    def decode_user_data(self,):
        """ Performs the decoding of the user data according to the signal type
        """
        ud = np.frombuffer(self.user_data, dtype=np.uint8)
        return ud

    @property
    def datation(self) -> float:
        """Perform the calculation of the ISP GPS time"""
        ct = self.packet_data_field.data_field_header.coarse_time
        ft = self.packet_data_field.data_field_header.fine_time * 2e8
        ft2 = self.packet_data_field.source_data.sar_header.datation_enhancement_service.fine_time_byte2
        ft += ft2
        # todo: using 1e6 in the code below is to cope for an error with the data. to be removed later
        fuso = F_USO * 1e6
        ft_sec = ft * 4 / fuso
        return GenericOrbitInterpolator.to_datetime64(ct + ft_sec)

    @property
    def sync_marker(self):
        return self.packet_data_field.source_data.sar_header.synchronisation_service.sync_marker

    @property
    def pri_counter(self) -> int:
        return self.packet_data_field.source_data.sar_header.index_service.pri_counter

    @property
    def data_field_header_size(self):
        return self.packet_data_field.data_field_header.size

    @property
    def sar_header_size(self):
        return self.packet_data_field.source_data.sar_header.size

    @property
    def pec_size(self):
        return self.packet_data_field.source_data.pec.size

    @property
    def pri(self) -> float:
        return self.packet_data_field.source_data.sar_header.real_time_parameter_service.pri / F_USO * 1e-6

    @property
    def prf(self) -> float:
        return 1/self.pri

    @property
    def swp(self) -> float:
        return self.packet_data_field.source_data.sar_header.real_time_parameter_service.swp / F_USO * 1e-6

    @property
    def swl(self) -> float:
        return self.packet_data_field.source_data.sar_header.real_time_parameter_service.swl / F_USO * 1e-6

    @property
    def rank(self) -> int:
        """ Return the rank
        """
        rank = self.packet_data_field.source_data.sar_header.real_time_parameter_service.rank
        return rank

    @property
    def baqmode(self) -> str:
        """ Return the BAQ mode
        """
        return self.packet_data_field.source_data.sar_header.real_time_parameter_service.baq_mode

    @property
    def baqbl(self) -> str:
        """ Return the BAQ mode
        """
        return self.packet_data_field.source_data.sar_header.data_take_information_service.baq_block_length

    @property
    def signal_type(self) -> str:
        """ Return the signal type
        """
        return self.packet_data_field.source_data.sar_header.real_time_parameter_service.signal_type

    @property
    def rx_polarisation(self) -> str:
        """
        retrieve the receive polarisation associated to the SAR ISP
        """
        if self.pid % 2 == 0:
            return 'H'
        else:
            return  'V'
    @property
    def tx_polarisation(self) -> str:
        #if self.pri_counter % 2 == 0:
            #even pri counnter means TxH
        #    self.packet_data_field_header['realtime_service']['tx_polarisation'] = 'H'
        #else :
        #    self.packet_data_field_header['realtime_service']['tx_polarisation'] = 'V'
        raise NotImplementedError


    def test_sync(self) -> bool:
        return self.sync_marker == SYNC_MARKER



