from dataclasses import dataclass, field, InitVar, asdict
from typing import Any
from abc import ABC, abstractmethod


import bpack
import bpack.bs
from bpack import T
BITS = bpack.EBaseUnits.BITS
BE = bpack.EByteOrder.BE

__all__ = ['GenericIsp', 'PacketHeader']


PACKET_HEADER_LENGTH = 6

@bpack.bs.decoder
@bpack.descriptor(baseunits=BITS, byteorder=BE)
class PacketHeader:
    version: T['u3'] = 0  # FIXED 0 means space packet
    packet_type: T['u1'] = 0  # FIXED 0 means telemetry packet
    data_field_header_flag: bool = True
    pid: T['u7'] = 0
    pcat: T['u4'] = 0
    grouping_flags: T['u2'] = 0
    sequence_count: T['u14'] = 0
    packet_data_field_length: T['u16'] = 0

    @property
    def size(self):
        return bpack.calcsize(self, bpack.EBaseUnits.BYTES)

    @property
    def packet_lenght(self):
        return self.packet_data_field_length

    def asdict(self):
        return bpack.asdict(self)


@dataclass
class GenericIsp:
    """ Top class contain defines a standard Source Packet
            |-----------------Source-Packet-----------------------|
            |--Packet-Header--|--------Packet-Data-Field----------|
    """
    packet_header: PacketHeader = PacketHeader()
    packet_data_field: Any = field(default=None, repr=False)


    @property
    def pid(self):
        return self.packet_header.pid

    @property
    def packet_header_size(self):
        return self.packet_header.size