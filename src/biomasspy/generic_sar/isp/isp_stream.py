import os
from collections import namedtuple

import logging

from .isp import GenericIsp, PacketHeader, PACKET_HEADER_LENGTH

format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")



class GenericIspStream():
    """Abstract class defining interface for decoding a stream of Instrument Source Packets (ISP).

    This class defines an API for ISP decoding.
    Opens the file handle makes it ready for reading

    Parameters
    ----------
    ispStreamFile : str
        path to the stream of ISPs


    Attributes
    -----------
    packet_header_length : int
        lenght of the primary packet header in bytes. Always 6 bytes

    """

    def __init__(self, ispStreamFile, limit=None):
        self.source = ispStreamFile
        self.fh = open (ispStreamFile, 'rb')

        #reading the fie size by poistioning the pointer at 0 byte offset from the end
        self.fh.seek(0, os.SEEK_END)
        self._file_size = self.fh.tell()
        self.fh.seek(0, os.SEEK_SET)

        #last decoded isp
        self.isp = None
        self._limit = limit


    def __del__(self):
        self.fh.close()

    def head(self):
        """
        Place thr file cursor at the start of the file handler
        """
        self.fh.seek(0, os.SEEK_SET)

    def pop(self, cls=GenericIsp):
        """
        Method for extracting the packet_primary_header and packet as binary elements.
        The primary packet header is decoded using the method decode_packet_header

        Rerurn
        ------
            packet_primary_header : dict
                dict containing all standard elements of the packet_primary_header
            packet : array
                array of bytes containing the packet_primary_header

        """
        #get the current position
        pos0 = self.fh.tell()

        #reading the necessary number of bytes
        data = self.fh.read(PACKET_HEADER_LENGTH)

        # decoding using bpack
        packet_header = PacketHeader.frombytes(data)

        packet_len = packet_header.packet_data_field_length + 1

        #reading packet data field as bytes
        packet = self.fh.read(packet_len)
        self.isp = cls(packet_header=packet_header, packet_data_field=packet)

        return self.isp



    def read(self,):
        """
        Simple method for looping (sequentially) over the ISP stream  until the end of the file

        Return
        ------
            packet_primary_header : array
                array of bytes containing the packet_primary_header
            packet : array
                array of bytes containing the packet_primary_header

        """
        i=0
        while (self.fh.tell() < self._file_size):
            try:
                self.isp = self.pop()
            except:
                logging.error('Error while decoding')



            print (self.isp)
            i += 1
            if self._limit is not None:
                if i > self._limit:
                    break

            #if i == 1000: break
