BIOMASS Source Packet unpacking library
===========================================

:Copyright: 2020-2021, Nuno Miranda, Michele Caccia, Antonio Valentino 

.. badges

|PyPI Status| |GHA Status| |Documentation Status|

.. description

The *biomasspy* Python package provides tools to describe and decode
BIOMASS Instrument Source Packet data.

The package is based on the *bpack* package allowing to efficiently decode data structures not
aligned with bytes bounday.

The package provides classes and functions that can be used to deocode:
* Generic ISPs 
* BIOMASS SAR Instrument Source Packet
* BIOMASS Ancillary Source Packet

The actual "dictionary" for BIOMASS packet decoding is developped by the ESA BIOMASS PDGS team.

