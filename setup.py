from setuptools import setup, find_packages

setup(name='biomasspy',
      version='0.1',
      description='Utils for decoding BIOMASS ISPs',
      url='https://gitlab.com/nuno.miranda/biomasspy',
      author='ESA BIOMASS PDGS team',
      author_email='nuno.miranda@example.com',
      license='MIT',
      packages=find_packages(where='src'),
      include_package_data=True,
      package_dir={"": "src"},
      #scripts=[''],
      zip_safe=False,
      python_requires='>=3.7')
