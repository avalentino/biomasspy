# all our targets are phony (no files to check).
.PHONY:

all: biomasspy

PYTHON = python3
PIP = pip3


biomasspy_dev:
	$(PYTHON) setup.py sdist bdist_wheel
	$(PIP) install -e . 

biomasspy:
	$(PYTHON) setup.py sdist bdist_wheel
	$(PIP) install --force-reinstall  dist/biomasspy-0.1-py3-none-any.whl

uninstall:
	$(PIP) uninstall biomasspy

clean:
	rm -rf dist build src/*egg-info
